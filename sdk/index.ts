/* tslint:disable */
/**
* @module SDKModule
* @author Jonathan Casarrubias <t:@johncasarrubias> <gh:jonathan-casarrubias>
* @license MIT 2016 Jonathan Casarrubias
* @version 2.1.0
* @description
* The SDKModule is a generated Software Development Kit automatically built by
* the LoopBack SDK Builder open source module.
*
* The SDKModule provides Angular 2 >= RC.5 support, which means that NgModules
* can import this Software Development Kit as follows:
*
*
* APP Route Module Context
* ============================================================================
* import { NgModule }       from '@angular/core';
* import { BrowserModule }  from '@angular/platform-browser';
* // App Root 
* import { AppComponent }   from './app.component';
* // Feature Modules
* import { SDK[Browser|Node|Native]Module } from './shared/sdk/sdk.module';
* // Import Routing
* import { routing }        from './app.routing';
* @NgModule({
*  imports: [
*    BrowserModule,
*    routing,
*    SDK[Browser|Node|Native]Module.forRoot()
*  ],
*  declarations: [ AppComponent ],
*  bootstrap:    [ AppComponent ]
* })
* export class AppModule { }
*
**/
import { JSONSearchParams } from './services/core/search.params';
import { ErrorHandler } from './services/core/error.service';
import { LoopBackAuth } from './services/core/auth.service';
import { LoggerService } from './services/custom/logger.service';
import { SDKModels } from './services/custom/SDKModels';
import { InternalStorage, SDKStorage } from './storage/storage.swaps';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CookieBrowser } from './storage/cookie.browser';
import { StorageBrowser } from './storage/storage.browser';
import { SocketBrowser } from './sockets/socket.browser';
import { SocketDriver } from './sockets/socket.driver';
import { SocketConnection } from './sockets/socket.connections';
import { RealTime } from './services/core/real.time';
import { CompanyApi } from './services/custom/Company';
import { IllnessApi } from './services/custom/Illness';
import { MedicationApi } from './services/custom/Medication';
import { AllergyApi } from './services/custom/Allergy';
import { BodyPartApi } from './services/custom/BodyPart';
import { EquipmentApi } from './services/custom/Equipment';
import { MovementApi } from './services/custom/Movement';
import { RoutineGoalApi } from './services/custom/RoutineGoal';
import { RoutineDifficultyApi } from './services/custom/RoutineDifficulty';
import { ArticulationApi } from './services/custom/Articulation';
import { ExerciseCategoryApi } from './services/custom/ExerciseCategory';
import { ExerciseApi } from './services/custom/Exercise';
import { RoutineApi } from './services/custom/Routine';
import { FoodApi } from './services/custom/Food';
import { DietApi } from './services/custom/Diet';
import { SuplierApi } from './services/custom/Suplier';
import { SaleGroupApi } from './services/custom/SaleGroup';
import { ProductCategoryApi } from './services/custom/ProductCategory';
import { PaymentMethodApi } from './services/custom/PaymentMethod';
import { IncomeCategoryApi } from './services/custom/IncomeCategory';
import { ProductApi } from './services/custom/Product';
import { ReceiptApi } from './services/custom/Receipt';
import { UsersApi } from './services/custom/Users';
import { MembershipTypesApi } from './services/custom/MembershipTypes';
import { InvoiceApi } from './services/custom/Invoice';
import { MembershipApi } from './services/custom/Membership';
import { EvaluationApi } from './services/custom/Evaluation';
import { CompanyIllnessApi } from './services/custom/CompanyIllness';
import { CompanyAllergyApi } from './services/custom/CompanyAllergy';
import { EmailApi } from './services/custom/Email';
import { BlogCategoryApi } from './services/custom/BlogCategory';
import { BlogApi } from './services/custom/Blog';
/**
* @module SDKBrowserModule
* @description
* This module should be imported when building a Web Application in the following scenarios:
*
*  1.- Regular web application
*  2.- Angular universal application (Browser Portion)
*  3.- Progressive applications (Angular Mobile, Ionic, WebViews, etc)
**/
@NgModule({
  imports:      [ CommonModule, HttpModule ],
  declarations: [ ],
  exports:      [ ],
  providers:    [
    ErrorHandler,
    SocketConnection
  ]
})
export class SDKBrowserModule {
  static forRoot(internalStorageProvider: any = {
    provide: InternalStorage,
    useClass: CookieBrowser
  }): ModuleWithProviders {
    return {
      ngModule  : SDKBrowserModule,
      providers : [
        LoopBackAuth,
        LoggerService,
        JSONSearchParams,
        SDKModels,
        RealTime,
        CompanyApi,
        IllnessApi,
        MedicationApi,
        AllergyApi,
        BodyPartApi,
        EquipmentApi,
        MovementApi,
        RoutineGoalApi,
        RoutineDifficultyApi,
        ArticulationApi,
        ExerciseCategoryApi,
        ExerciseApi,
        RoutineApi,
        FoodApi,
        DietApi,
        SuplierApi,
        SaleGroupApi,
        ProductCategoryApi,
        PaymentMethodApi,
        IncomeCategoryApi,
        ProductApi,
        ReceiptApi,
        UsersApi,
        MembershipTypesApi,
        InvoiceApi,
        MembershipApi,
        EvaluationApi,
        CompanyIllnessApi,
        CompanyAllergyApi,
        EmailApi,
        BlogCategoryApi,
        BlogApi,
        internalStorageProvider,
        { provide: SDKStorage, useClass: StorageBrowser },
        { provide: SocketDriver, useClass: SocketBrowser }
      ]
    };
  }
}
/**
* Have Fun!!!
* - Jon
**/
export * from './models/index';
export * from './services/index';
export * from './lb.config';
export * from './storage/storage.swaps';
export { CookieBrowser } from './storage/cookie.browser';
export { StorageBrowser } from './storage/storage.browser';

