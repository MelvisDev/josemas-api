/* tslint:disable */

declare var Object: any;
export interface CompanyAllergyInterface {
  "id"?: any;
}

export class CompanyAllergy implements CompanyAllergyInterface {
  "id": any;
  constructor(data?: CompanyAllergyInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `CompanyAllergy`.
   */
  public static getModelName() {
    return "CompanyAllergy";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of CompanyAllergy for dynamic purposes.
  **/
  public static factory(data: CompanyAllergyInterface): CompanyAllergy{
    return new CompanyAllergy(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'CompanyAllergy',
      plural: 'CompanyAllergies',
      path: 'CompanyAllergies',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
      },
      relations: {
      }
    }
  }
}
