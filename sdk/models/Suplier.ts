/* tslint:disable */
import {
  Company
} from '../index';

declare var Object: any;
export interface SuplierInterface {
  "name"?: string;
  "logoUrl"?: string;
  "phoneNumber"?: string;
  "website"?: string;
  "id"?: string;
  "contacts"?: Array<any>;
  "companyId"?: string;
  "createdDate"?: Date;
  "createdBy"?: number;
  "lastModificationDate"?: Date;
  "lastModificationBy"?: number;
  _contacts?: any[];
  company?: Company;
  productSuppliers?: any[];
}

export class Suplier implements SuplierInterface {
  "name": string;
  "logoUrl": string;
  "phoneNumber": string;
  "website": string;
  "id": string;
  "contacts": Array<any>;
  "companyId": string;
  "createdDate": Date;
  "createdBy": number;
  "lastModificationDate": Date;
  "lastModificationBy": number;
  _contacts: any[];
  company: Company;
  productSuppliers: any[];
  constructor(data?: SuplierInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Suplier`.
   */
  public static getModelName() {
    return "Suplier";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Suplier for dynamic purposes.
  **/
  public static factory(data: SuplierInterface): Suplier{
    return new Suplier(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Suplier',
      plural: 'Supliers',
      path: 'Supliers',
      idName: 'id',
      properties: {
        "name": {
          name: 'name',
          type: 'string'
        },
        "logoUrl": {
          name: 'logoUrl',
          type: 'string'
        },
        "phoneNumber": {
          name: 'phoneNumber',
          type: 'string'
        },
        "website": {
          name: 'website',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'string'
        },
        "contacts": {
          name: 'contacts',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "companyId": {
          name: 'companyId',
          type: 'string'
        },
        "createdDate": {
          name: 'createdDate',
          type: 'Date',
          default: new Date(0)
        },
        "createdBy": {
          name: 'createdBy',
          type: 'number',
          default: 0
        },
        "lastModificationDate": {
          name: 'lastModificationDate',
          type: 'Date',
          default: new Date(0)
        },
        "lastModificationBy": {
          name: 'lastModificationBy',
          type: 'number',
          default: 0
        },
      },
      relations: {
        _contacts: {
          name: '_contacts',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'contacts',
          keyTo: 'id'
        },
        company: {
          name: 'company',
          type: 'Company',
          model: 'Company',
          relationType: 'belongsTo',
                  keyFrom: 'companyId',
          keyTo: 'id'
        },
        productSuppliers: {
          name: 'productSuppliers',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'suplierId'
        },
      }
    }
  }
}
