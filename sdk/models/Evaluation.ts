/* tslint:disable */
import {
  Users
} from '../index';

declare var Object: any;
export interface EvaluationInterface {
  "id"?: string;
  "usersId"?: string;
  "evaluations"?: Array<any>;
  "createdDate"?: Date;
  "createdBy"?: number;
  "lastModificationDate"?: Date;
  "lastModificationBy"?: number;
  users?: Users;
  _evaluations?: any[];
}

export class Evaluation implements EvaluationInterface {
  "id": string;
  "usersId": string;
  "evaluations": Array<any>;
  "createdDate": Date;
  "createdBy": number;
  "lastModificationDate": Date;
  "lastModificationBy": number;
  users: Users;
  _evaluations: any[];
  constructor(data?: EvaluationInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Evaluation`.
   */
  public static getModelName() {
    return "Evaluation";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Evaluation for dynamic purposes.
  **/
  public static factory(data: EvaluationInterface): Evaluation{
    return new Evaluation(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Evaluation',
      plural: 'Evaluations',
      path: 'Evaluations',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'string'
        },
        "usersId": {
          name: 'usersId',
          type: 'string'
        },
        "evaluations": {
          name: 'evaluations',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "createdDate": {
          name: 'createdDate',
          type: 'Date',
          default: new Date(0)
        },
        "createdBy": {
          name: 'createdBy',
          type: 'number',
          default: 0
        },
        "lastModificationDate": {
          name: 'lastModificationDate',
          type: 'Date',
          default: new Date(0)
        },
        "lastModificationBy": {
          name: 'lastModificationBy',
          type: 'number',
          default: 0
        },
      },
      relations: {
        users: {
          name: 'users',
          type: 'Users',
          model: 'Users',
          relationType: 'belongsTo',
                  keyFrom: 'usersId',
          keyTo: 'id'
        },
        _evaluations: {
          name: '_evaluations',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'evaluations',
          keyTo: 'id'
        },
      }
    }
  }
}
