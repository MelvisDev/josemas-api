/* tslint:disable */
import {
  Company,
  Users
} from '../index';

declare var Object: any;
export interface MembershipInterface {
  "amount"?: number;
  "id"?: string;
  "companyId"?: string;
  "customerId"?: string;
  "access"?: any;
  "duration"?: any;
  "usersId"?: string;
  "createdDate"?: Date;
  "createdBy"?: number;
  "lastModificationDate"?: Date;
  "lastModificationBy"?: number;
  company?: Company;
  users?: Users;
  _access?: any[];
  _duration?: any[];
}

export class Membership implements MembershipInterface {
  "amount": number;
  "id": string;
  "companyId": string;
  "customerId": string;
  "access": any;
  "duration": any;
  "usersId": string;
  "createdDate": Date;
  "createdBy": number;
  "lastModificationDate": Date;
  "lastModificationBy": number;
  company: Company;
  users: Users;
  _access: any[];
  _duration: any[];
  constructor(data?: MembershipInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Membership`.
   */
  public static getModelName() {
    return "Membership";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Membership for dynamic purposes.
  **/
  public static factory(data: MembershipInterface): Membership{
    return new Membership(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Membership',
      plural: 'Memberships',
      path: 'Memberships',
      idName: 'id',
      properties: {
        "amount": {
          name: 'amount',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'string'
        },
        "companyId": {
          name: 'companyId',
          type: 'string'
        },
        "customerId": {
          name: 'customerId',
          type: 'string'
        },
        "access": {
          name: 'access',
          type: 'any'
        },
        "duration": {
          name: 'duration',
          type: 'any'
        },
        "usersId": {
          name: 'usersId',
          type: 'string'
        },
        "createdDate": {
          name: 'createdDate',
          type: 'Date',
          default: new Date(0)
        },
        "createdBy": {
          name: 'createdBy',
          type: 'number',
          default: 0
        },
        "lastModificationDate": {
          name: 'lastModificationDate',
          type: 'Date',
          default: new Date(0)
        },
        "lastModificationBy": {
          name: 'lastModificationBy',
          type: 'number',
          default: 0
        },
      },
      relations: {
        company: {
          name: 'company',
          type: 'Company',
          model: 'Company',
          relationType: 'belongsTo',
                  keyFrom: 'companyId',
          keyTo: 'id'
        },
        users: {
          name: 'users',
          type: 'Users',
          model: 'Users',
          relationType: 'belongsTo',
                  keyFrom: 'customerId',
          keyTo: 'id'
        },
        _access: {
          name: '_access',
          type: 'any[]',
          model: '',
          relationType: 'embedsOne',
                  keyFrom: 'access',
          keyTo: 'id'
        },
        _duration: {
          name: '_duration',
          type: 'any[]',
          model: '',
          relationType: 'embedsOne',
                  keyFrom: 'duration',
          keyTo: 'id'
        },
      }
    }
  }
}
