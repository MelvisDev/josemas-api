/* tslint:disable */
import {
  BlogCategory
} from '../index';

declare var Object: any;
export interface BlogInterface {
  "publishDate"?: Date;
  "published"?: boolean;
  "approvedBy"?: string;
  "id"?: string;
  "blogCategoryId"?: string;
  "createdDate"?: Date;
  "createdBy"?: number;
  "lastModificationDate"?: Date;
  "lastModificationBy"?: number;
  "languages"?: Array<any>;
  blogCategory?: BlogCategory;
  _languages?: any[];
}

export class Blog implements BlogInterface {
  "publishDate": Date;
  "published": boolean;
  "approvedBy": string;
  "id": string;
  "blogCategoryId": string;
  "createdDate": Date;
  "createdBy": number;
  "lastModificationDate": Date;
  "lastModificationBy": number;
  "languages": Array<any>;
  blogCategory: BlogCategory;
  _languages: any[];
  constructor(data?: BlogInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Blog`.
   */
  public static getModelName() {
    return "Blog";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Blog for dynamic purposes.
  **/
  public static factory(data: BlogInterface): Blog{
    return new Blog(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Blog',
      plural: 'Blogs',
      path: 'Blogs',
      idName: 'id',
      properties: {
        "publishDate": {
          name: 'publishDate',
          type: 'Date'
        },
        "published": {
          name: 'published',
          type: 'boolean'
        },
        "approvedBy": {
          name: 'approvedBy',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'string'
        },
        "blogCategoryId": {
          name: 'blogCategoryId',
          type: 'string'
        },
        "createdDate": {
          name: 'createdDate',
          type: 'Date',
          default: new Date(0)
        },
        "createdBy": {
          name: 'createdBy',
          type: 'number',
          default: 0
        },
        "lastModificationDate": {
          name: 'lastModificationDate',
          type: 'Date',
          default: new Date(0)
        },
        "lastModificationBy": {
          name: 'lastModificationBy',
          type: 'number',
          default: 0
        },
        "languages": {
          name: 'languages',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
      },
      relations: {
        blogCategory: {
          name: 'blogCategory',
          type: 'BlogCategory',
          model: 'BlogCategory',
          relationType: 'belongsTo',
                  keyFrom: 'blogCategoryId',
          keyTo: 'id'
        },
        _languages: {
          name: '_languages',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'languages',
          keyTo: 'id'
        },
      }
    }
  }
}
