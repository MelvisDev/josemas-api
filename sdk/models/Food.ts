/* tslint:disable */
import {
  Company
} from '../index';

declare var Object: any;
export interface FoodInterface {
  "id"?: string;
  "imageUrl"?: string;
  "companyId"?: string;
  "languages"?: Array<any>;
  "nutritionFacts"?: any;
  "createdDate"?: Date;
  "createdBy"?: number;
  "lastModificationDate"?: Date;
  "lastModificationBy"?: number;
  company?: Company;
  _languages?: any[];
  _nutritionFacts?: any[];
}

export class Food implements FoodInterface {
  "id": string;
  "imageUrl": string;
  "companyId": string;
  "languages": Array<any>;
  "nutritionFacts": any;
  "createdDate": Date;
  "createdBy": number;
  "lastModificationDate": Date;
  "lastModificationBy": number;
  company: Company;
  _languages: any[];
  _nutritionFacts: any[];
  constructor(data?: FoodInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Food`.
   */
  public static getModelName() {
    return "Food";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Food for dynamic purposes.
  **/
  public static factory(data: FoodInterface): Food{
    return new Food(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Food',
      plural: 'foods',
      path: 'foods',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'string'
        },
        "imageUrl": {
          name: 'imageUrl',
          type: 'string'
        },
        "companyId": {
          name: 'companyId',
          type: 'string'
        },
        "languages": {
          name: 'languages',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "nutritionFacts": {
          name: 'nutritionFacts',
          type: 'any'
        },
        "createdDate": {
          name: 'createdDate',
          type: 'Date',
          default: new Date(0)
        },
        "createdBy": {
          name: 'createdBy',
          type: 'number',
          default: 0
        },
        "lastModificationDate": {
          name: 'lastModificationDate',
          type: 'Date',
          default: new Date(0)
        },
        "lastModificationBy": {
          name: 'lastModificationBy',
          type: 'number',
          default: 0
        },
      },
      relations: {
        company: {
          name: 'company',
          type: 'Company',
          model: 'Company',
          relationType: 'belongsTo',
                  keyFrom: 'companyId',
          keyTo: 'id'
        },
        _languages: {
          name: '_languages',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'languages',
          keyTo: 'id'
        },
        _nutritionFacts: {
          name: '_nutritionFacts',
          type: 'any[]',
          model: '',
          relationType: 'embedsOne',
                  keyFrom: 'nutritionFacts',
          keyTo: 'id'
        },
      }
    }
  }
}
