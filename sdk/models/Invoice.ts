/* tslint:disable */
import {
  Company,
  Users
} from '../index';

declare var Object: any;
export interface InvoiceInterface {
  "date"?: Date;
  "amount"?: number;
  "seller"?: string;
  "tax"?: number;
  "total"?: number;
  "balance"?: number;
  "id"?: string;
  "companyId"?: string;
  "customerId"?: string;
  "details"?: any;
  "usersId"?: string;
  "createdDate"?: Date;
  "createdBy"?: number;
  "lastModificationDate"?: Date;
  "lastModificationBy"?: number;
  company?: Company;
  users?: Users;
  _details?: any[];
}

export class Invoice implements InvoiceInterface {
  "date": Date;
  "amount": number;
  "seller": string;
  "tax": number;
  "total": number;
  "balance": number;
  "id": string;
  "companyId": string;
  "customerId": string;
  "details": any;
  "usersId": string;
  "createdDate": Date;
  "createdBy": number;
  "lastModificationDate": Date;
  "lastModificationBy": number;
  company: Company;
  users: Users;
  _details: any[];
  constructor(data?: InvoiceInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Invoice`.
   */
  public static getModelName() {
    return "Invoice";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Invoice for dynamic purposes.
  **/
  public static factory(data: InvoiceInterface): Invoice{
    return new Invoice(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Invoice',
      plural: 'Invoices',
      path: 'Invoices',
      idName: 'id',
      properties: {
        "date": {
          name: 'date',
          type: 'Date'
        },
        "amount": {
          name: 'amount',
          type: 'number'
        },
        "seller": {
          name: 'seller',
          type: 'string'
        },
        "tax": {
          name: 'tax',
          type: 'number'
        },
        "total": {
          name: 'total',
          type: 'number'
        },
        "balance": {
          name: 'balance',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'string'
        },
        "companyId": {
          name: 'companyId',
          type: 'string'
        },
        "customerId": {
          name: 'customerId',
          type: 'string'
        },
        "details": {
          name: 'details',
          type: 'any'
        },
        "usersId": {
          name: 'usersId',
          type: 'string'
        },
        "createdDate": {
          name: 'createdDate',
          type: 'Date',
          default: new Date(0)
        },
        "createdBy": {
          name: 'createdBy',
          type: 'number',
          default: 0
        },
        "lastModificationDate": {
          name: 'lastModificationDate',
          type: 'Date',
          default: new Date(0)
        },
        "lastModificationBy": {
          name: 'lastModificationBy',
          type: 'number',
          default: 0
        },
      },
      relations: {
        company: {
          name: 'company',
          type: 'Company',
          model: 'Company',
          relationType: 'belongsTo',
                  keyFrom: 'companyId',
          keyTo: 'id'
        },
        users: {
          name: 'users',
          type: 'Users',
          model: 'Users',
          relationType: 'belongsTo',
                  keyFrom: 'customerId',
          keyTo: 'id'
        },
        _details: {
          name: '_details',
          type: 'any[]',
          model: '',
          relationType: 'embedsOne',
                  keyFrom: 'details',
          keyTo: 'id'
        },
      }
    }
  }
}
