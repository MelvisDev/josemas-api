/* tslint:disable */
import {
  Medication,
  BodyPart,
  Food,
  Diet,
  Suplier,
  SaleGroup,
  ProductCategory,
  PaymentMethod,
  IncomeCategory,
  Product,
  Receipt,
  Users,
  MembershipTypes,
  Invoice,
  Membership,
  Illness,
  Allergy,
  BlogCategory,
  GeoPoint
} from '../index';

declare var Object: any;
export interface CompanyInterface {
  "name": string;
  "address"?: string;
  "logoUrl"?: string;
  "images"?: Array<any>;
  "location"?: GeoPoint;
  "email": string;
  "administrativeMail": string;
  "phoneNumber"?: string;
  "celphone"?: string;
  "documentNumber"?: string;
  "birthdate"?: Date;
  "facebook"?: string;
  "instagram"?: string;
  "youtube"?: string;
  "id"?: string;
  "workingHour"?: Array<any>;
  "exceptionDay"?: Array<any>;
  "createdDate"?: Date;
  "createdBy"?: number;
  "lastModificationDate"?: Date;
  "lastModificationBy"?: number;
  "license"?: any;
  "notifications"?: any;
  "equipments"?: Array<any>;
  workingHours?: any[];
  exceptionDays?: any[];
  medications?: Medication[];
  bodyParts?: BodyPart[];
  food?: Food[];
  diets?: Diet[];
  supliers?: Suplier[];
  saleGroups?: SaleGroup[];
  productCategories?: ProductCategory[];
  paymentMethods?: PaymentMethod[];
  incomeCategories?: IncomeCategory[];
  products?: Product[];
  receipts?: Receipt[];
  users?: Users[];
  membershipTypes?: MembershipTypes[];
  invoices?: Invoice[];
  memberships?: Membership[];
  illnesses?: Illness[];
  allergies?: Allergy[];
  _license?: any[];
  _notifications?: any[];
  _configuration?: any[];
  _equipment?: any[];
  blogCategories?: BlogCategory[];
}

export class Company implements CompanyInterface {
  "name": string;
  "address": string;
  "logoUrl": string;
  "images": Array<any>;
  "location": GeoPoint;
  "email": string;
  "administrativeMail": string;
  "phoneNumber": string;
  "celphone": string;
  "documentNumber": string;
  "birthdate": Date;
  "facebook": string;
  "instagram": string;
  "youtube": string;
  "id": string;
  "workingHour": Array<any>;
  "exceptionDay": Array<any>;
  "createdDate": Date;
  "createdBy": number;
  "lastModificationDate": Date;
  "lastModificationBy": number;
  "license": any;
  "notifications": any;
  "equipments": Array<any>;
  workingHours: any[];
  exceptionDays: any[];
  medications: Medication[];
  bodyParts: BodyPart[];
  food: Food[];
  diets: Diet[];
  supliers: Suplier[];
  saleGroups: SaleGroup[];
  productCategories: ProductCategory[];
  paymentMethods: PaymentMethod[];
  incomeCategories: IncomeCategory[];
  products: Product[];
  receipts: Receipt[];
  users: Users[];
  membershipTypes: MembershipTypes[];
  invoices: Invoice[];
  memberships: Membership[];
  illnesses: Illness[];
  allergies: Allergy[];
  _license: any[];
  _notifications: any[];
  _configuration: any[];
  _equipment: any[];
  blogCategories: BlogCategory[];
  constructor(data?: CompanyInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Company`.
   */
  public static getModelName() {
    return "Company";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Company for dynamic purposes.
  **/
  public static factory(data: CompanyInterface): Company{
    return new Company(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Company',
      plural: 'Companies',
      path: 'Companies',
      idName: 'id',
      properties: {
        "name": {
          name: 'name',
          type: 'string'
        },
        "address": {
          name: 'address',
          type: 'string'
        },
        "logoUrl": {
          name: 'logoUrl',
          type: 'string'
        },
        "images": {
          name: 'images',
          type: 'Array&lt;any&gt;'
        },
        "location": {
          name: 'location',
          type: 'GeoPoint'
        },
        "email": {
          name: 'email',
          type: 'string'
        },
        "administrativeMail": {
          name: 'administrativeMail',
          type: 'string'
        },
        "phoneNumber": {
          name: 'phoneNumber',
          type: 'string'
        },
        "celphone": {
          name: 'celphone',
          type: 'string'
        },
        "documentNumber": {
          name: 'documentNumber',
          type: 'string'
        },
        "birthdate": {
          name: 'birthdate',
          type: 'Date'
        },
        "facebook": {
          name: 'facebook',
          type: 'string'
        },
        "instagram": {
          name: 'instagram',
          type: 'string'
        },
        "youtube": {
          name: 'youtube',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'string'
        },
        "workingHour": {
          name: 'workingHour',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "exceptionDay": {
          name: 'exceptionDay',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "createdDate": {
          name: 'createdDate',
          type: 'Date',
          default: new Date(0)
        },
        "createdBy": {
          name: 'createdBy',
          type: 'number',
          default: 0
        },
        "lastModificationDate": {
          name: 'lastModificationDate',
          type: 'Date',
          default: new Date(0)
        },
        "lastModificationBy": {
          name: 'lastModificationBy',
          type: 'number',
          default: 0
        },
        "license": {
          name: 'license',
          type: 'any'
        },
        "notifications": {
          name: 'notifications',
          type: 'any'
        },
        "equipments": {
          name: 'equipments',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
      },
      relations: {
        workingHours: {
          name: 'workingHours',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'workingHour',
          keyTo: 'id'
        },
        exceptionDays: {
          name: 'exceptionDays',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'exceptionDay',
          keyTo: 'id'
        },
        medications: {
          name: 'medications',
          type: 'Medication[]',
          model: 'Medication',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'companyId'
        },
        bodyParts: {
          name: 'bodyParts',
          type: 'BodyPart[]',
          model: 'BodyPart',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'companyId'
        },
        food: {
          name: 'food',
          type: 'Food[]',
          model: 'Food',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'companyId'
        },
        diets: {
          name: 'diets',
          type: 'Diet[]',
          model: 'Diet',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'companyId'
        },
        supliers: {
          name: 'supliers',
          type: 'Suplier[]',
          model: 'Suplier',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'companyId'
        },
        saleGroups: {
          name: 'saleGroups',
          type: 'SaleGroup[]',
          model: 'SaleGroup',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'companyId'
        },
        productCategories: {
          name: 'productCategories',
          type: 'ProductCategory[]',
          model: 'ProductCategory',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'companyId'
        },
        paymentMethods: {
          name: 'paymentMethods',
          type: 'PaymentMethod[]',
          model: 'PaymentMethod',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'companyId'
        },
        incomeCategories: {
          name: 'incomeCategories',
          type: 'IncomeCategory[]',
          model: 'IncomeCategory',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'companyId'
        },
        products: {
          name: 'products',
          type: 'Product[]',
          model: 'Product',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'companyId'
        },
        receipts: {
          name: 'receipts',
          type: 'Receipt[]',
          model: 'Receipt',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'companyId'
        },
        users: {
          name: 'users',
          type: 'Users[]',
          model: 'Users',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'companyId'
        },
        membershipTypes: {
          name: 'membershipTypes',
          type: 'MembershipTypes[]',
          model: 'MembershipTypes',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'companyId'
        },
        invoices: {
          name: 'invoices',
          type: 'Invoice[]',
          model: 'Invoice',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'companyId'
        },
        memberships: {
          name: 'memberships',
          type: 'Membership[]',
          model: 'Membership',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'companyId'
        },
        illnesses: {
          name: 'illnesses',
          type: 'Illness[]',
          model: 'Illness',
          relationType: 'hasMany',
          modelThrough: 'CompanyIllness',
          keyThrough: 'illnessId',
          keyFrom: 'id',
          keyTo: 'companyId'
        },
        allergies: {
          name: 'allergies',
          type: 'Allergy[]',
          model: 'Allergy',
          relationType: 'hasMany',
          modelThrough: 'CompanyAllergy',
          keyThrough: 'allergyId',
          keyFrom: 'id',
          keyTo: 'companyId'
        },
        _license: {
          name: '_license',
          type: 'any[]',
          model: '',
          relationType: 'embedsOne',
                  keyFrom: 'license',
          keyTo: 'id'
        },
        _notifications: {
          name: '_notifications',
          type: 'any[]',
          model: '',
          relationType: 'embedsOne',
                  keyFrom: 'notifications',
          keyTo: 'id'
        },
        _configuration: {
          name: '_configuration',
          type: 'any[]',
          model: '',
          relationType: 'embedsOne',
                  keyFrom: 'notifications',
          keyTo: 'id'
        },
        _equipment: {
          name: '_equipment',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'equipments',
          keyTo: 'id'
        },
        blogCategories: {
          name: 'blogCategories',
          type: 'BlogCategory[]',
          model: 'BlogCategory',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'companyId'
        },
      }
    }
  }
}
