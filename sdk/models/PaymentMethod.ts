/* tslint:disable */
import {
  Company
} from '../index';

declare var Object: any;
export interface PaymentMethodInterface {
  "name"?: string;
  "isAutoValidated"?: boolean;
  "id"?: string;
  "companyId"?: string;
  "createdDate"?: Date;
  "createdBy"?: number;
  "lastModificationDate"?: Date;
  "lastModificationBy"?: number;
  company?: Company;
}

export class PaymentMethod implements PaymentMethodInterface {
  "name": string;
  "isAutoValidated": boolean;
  "id": string;
  "companyId": string;
  "createdDate": Date;
  "createdBy": number;
  "lastModificationDate": Date;
  "lastModificationBy": number;
  company: Company;
  constructor(data?: PaymentMethodInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `PaymentMethod`.
   */
  public static getModelName() {
    return "PaymentMethod";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of PaymentMethod for dynamic purposes.
  **/
  public static factory(data: PaymentMethodInterface): PaymentMethod{
    return new PaymentMethod(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'PaymentMethod',
      plural: 'PaymentMethods',
      path: 'PaymentMethods',
      idName: 'id',
      properties: {
        "name": {
          name: 'name',
          type: 'string'
        },
        "isAutoValidated": {
          name: 'isAutoValidated',
          type: 'boolean'
        },
        "id": {
          name: 'id',
          type: 'string'
        },
        "companyId": {
          name: 'companyId',
          type: 'string'
        },
        "createdDate": {
          name: 'createdDate',
          type: 'Date',
          default: new Date(0)
        },
        "createdBy": {
          name: 'createdBy',
          type: 'number',
          default: 0
        },
        "lastModificationDate": {
          name: 'lastModificationDate',
          type: 'Date',
          default: new Date(0)
        },
        "lastModificationBy": {
          name: 'lastModificationBy',
          type: 'number',
          default: 0
        },
      },
      relations: {
        company: {
          name: 'company',
          type: 'Company',
          model: 'Company',
          relationType: 'belongsTo',
                  keyFrom: 'companyId',
          keyTo: 'id'
        },
      }
    }
  }
}
