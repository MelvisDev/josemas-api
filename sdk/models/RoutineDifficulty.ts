/* tslint:disable */

declare var Object: any;
export interface RoutineDifficultyInterface {
  "id"?: string;
  "createdDate"?: Date;
  "createdBy"?: number;
  "lastModificationDate"?: Date;
  "lastModificationBy"?: number;
  "languages"?: Array<any>;
  language?: any[];
}

export class RoutineDifficulty implements RoutineDifficultyInterface {
  "id": string;
  "createdDate": Date;
  "createdBy": number;
  "lastModificationDate": Date;
  "lastModificationBy": number;
  "languages": Array<any>;
  language: any[];
  constructor(data?: RoutineDifficultyInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `RoutineDifficulty`.
   */
  public static getModelName() {
    return "RoutineDifficulty";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of RoutineDifficulty for dynamic purposes.
  **/
  public static factory(data: RoutineDifficultyInterface): RoutineDifficulty{
    return new RoutineDifficulty(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'RoutineDifficulty',
      plural: 'RoutineDifficulties',
      path: 'RoutineDifficulties',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'string'
        },
        "createdDate": {
          name: 'createdDate',
          type: 'Date',
          default: new Date(0)
        },
        "createdBy": {
          name: 'createdBy',
          type: 'number',
          default: 0
        },
        "lastModificationDate": {
          name: 'lastModificationDate',
          type: 'Date',
          default: new Date(0)
        },
        "lastModificationBy": {
          name: 'lastModificationBy',
          type: 'number',
          default: 0
        },
        "languages": {
          name: 'languages',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
      },
      relations: {
        language: {
          name: 'language',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'languages',
          keyTo: 'id'
        },
      }
    }
  }
}
