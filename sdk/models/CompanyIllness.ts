/* tslint:disable */

declare var Object: any;
export interface CompanyIllnessInterface {
  "id"?: any;
}

export class CompanyIllness implements CompanyIllnessInterface {
  "id": any;
  constructor(data?: CompanyIllnessInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `CompanyIllness`.
   */
  public static getModelName() {
    return "CompanyIllness";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of CompanyIllness for dynamic purposes.
  **/
  public static factory(data: CompanyIllnessInterface): CompanyIllness{
    return new CompanyIllness(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'CompanyIllness',
      plural: 'CompanyIllnesses',
      path: 'CompanyIllnesses',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
      },
      relations: {
      }
    }
  }
}
