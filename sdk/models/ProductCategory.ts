/* tslint:disable */
import {
  Company,
  Product
} from '../index';

declare var Object: any;
export interface ProductCategoryInterface {
  "name"?: string;
  "description"?: string;
  "imageUrl"?: string;
  "id"?: string;
  "companyId"?: string;
  "createdDate"?: Date;
  "createdBy"?: number;
  "lastModificationDate"?: Date;
  "lastModificationBy"?: number;
  company?: Company;
  products?: Product[];
}

export class ProductCategory implements ProductCategoryInterface {
  "name": string;
  "description": string;
  "imageUrl": string;
  "id": string;
  "companyId": string;
  "createdDate": Date;
  "createdBy": number;
  "lastModificationDate": Date;
  "lastModificationBy": number;
  company: Company;
  products: Product[];
  constructor(data?: ProductCategoryInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `ProductCategory`.
   */
  public static getModelName() {
    return "ProductCategory";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of ProductCategory for dynamic purposes.
  **/
  public static factory(data: ProductCategoryInterface): ProductCategory{
    return new ProductCategory(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'ProductCategory',
      plural: 'ProductCategories',
      path: 'ProductCategories',
      idName: 'id',
      properties: {
        "name": {
          name: 'name',
          type: 'string'
        },
        "description": {
          name: 'description',
          type: 'string'
        },
        "imageUrl": {
          name: 'imageUrl',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'string'
        },
        "companyId": {
          name: 'companyId',
          type: 'string'
        },
        "createdDate": {
          name: 'createdDate',
          type: 'Date',
          default: new Date(0)
        },
        "createdBy": {
          name: 'createdBy',
          type: 'number',
          default: 0
        },
        "lastModificationDate": {
          name: 'lastModificationDate',
          type: 'Date',
          default: new Date(0)
        },
        "lastModificationBy": {
          name: 'lastModificationBy',
          type: 'number',
          default: 0
        },
      },
      relations: {
        company: {
          name: 'company',
          type: 'Company',
          model: 'Company',
          relationType: 'belongsTo',
                  keyFrom: 'companyId',
          keyTo: 'id'
        },
        products: {
          name: 'products',
          type: 'Product[]',
          model: 'Product',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'productCategoryId'
        },
      }
    }
  }
}
