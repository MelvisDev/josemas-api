/* tslint:disable */
import {
  Company
} from '../index';

declare var Object: any;
export interface IllnessInterface {
  "id"?: string;
  "languages"?: Array<any>;
  "companyId"?: string;
  "createdDate"?: Date;
  "createdBy"?: number;
  "lastModificationDate"?: Date;
  "lastModificationBy"?: number;
  language?: any[];
  company?: Company;
  companies?: Company[];
}

export class Illness implements IllnessInterface {
  "id": string;
  "languages": Array<any>;
  "companyId": string;
  "createdDate": Date;
  "createdBy": number;
  "lastModificationDate": Date;
  "lastModificationBy": number;
  language: any[];
  company: Company;
  companies: Company[];
  constructor(data?: IllnessInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Illness`.
   */
  public static getModelName() {
    return "Illness";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Illness for dynamic purposes.
  **/
  public static factory(data: IllnessInterface): Illness{
    return new Illness(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Illness',
      plural: 'Illnesses',
      path: 'Illnesses',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'string'
        },
        "languages": {
          name: 'languages',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "companyId": {
          name: 'companyId',
          type: 'string'
        },
        "createdDate": {
          name: 'createdDate',
          type: 'Date',
          default: new Date(0)
        },
        "createdBy": {
          name: 'createdBy',
          type: 'number',
          default: 0
        },
        "lastModificationDate": {
          name: 'lastModificationDate',
          type: 'Date',
          default: new Date(0)
        },
        "lastModificationBy": {
          name: 'lastModificationBy',
          type: 'number',
          default: 0
        },
      },
      relations: {
        language: {
          name: 'language',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'languages',
          keyTo: 'id'
        },
        company: {
          name: 'company',
          type: 'Company',
          model: 'Company',
          relationType: 'belongsTo',
                  keyFrom: 'companyId',
          keyTo: 'id'
        },
        companies: {
          name: 'companies',
          type: 'Company[]',
          model: 'Company',
          relationType: 'hasMany',
          modelThrough: 'CompanyIllness',
          keyThrough: 'companyId',
          keyFrom: 'id',
          keyTo: 'illnessId'
        },
      }
    }
  }
}
