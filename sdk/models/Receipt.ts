/* tslint:disable */
import {
  PaymentMethod,
  Company,
  Users
} from '../index';

declare var Object: any;
export interface ReceiptInterface {
  "paymentDate"?: Date;
  "validated"?: boolean;
  "id"?: string;
  "paymentMethod"?: any;
  "companyId"?: string;
  "createdDate"?: Date;
  "createdBy"?: number;
  "lastModificationDate"?: Date;
  "lastModificationBy"?: number;
  "usersId"?: string;
  "validateBy"?: string;
  _paymentMethod?: PaymentMethod[];
  company?: Company;
  users?: Users;
}

export class Receipt implements ReceiptInterface {
  "paymentDate": Date;
  "validated": boolean;
  "id": string;
  "paymentMethod": any;
  "companyId": string;
  "createdDate": Date;
  "createdBy": number;
  "lastModificationDate": Date;
  "lastModificationBy": number;
  "usersId": string;
  "validateBy": string;
  _paymentMethod: PaymentMethod[];
  company: Company;
  users: Users;
  constructor(data?: ReceiptInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Receipt`.
   */
  public static getModelName() {
    return "Receipt";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Receipt for dynamic purposes.
  **/
  public static factory(data: ReceiptInterface): Receipt{
    return new Receipt(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Receipt',
      plural: 'Receipts',
      path: 'Receipts',
      idName: 'id',
      properties: {
        "paymentDate": {
          name: 'paymentDate',
          type: 'Date'
        },
        "validated": {
          name: 'validated',
          type: 'boolean'
        },
        "id": {
          name: 'id',
          type: 'string'
        },
        "paymentMethod": {
          name: 'paymentMethod',
          type: 'any'
        },
        "companyId": {
          name: 'companyId',
          type: 'string'
        },
        "createdDate": {
          name: 'createdDate',
          type: 'Date',
          default: new Date(0)
        },
        "createdBy": {
          name: 'createdBy',
          type: 'number',
          default: 0
        },
        "lastModificationDate": {
          name: 'lastModificationDate',
          type: 'Date',
          default: new Date(0)
        },
        "lastModificationBy": {
          name: 'lastModificationBy',
          type: 'number',
          default: 0
        },
        "usersId": {
          name: 'usersId',
          type: 'string'
        },
        "validateBy": {
          name: 'validateBy',
          type: 'string'
        },
      },
      relations: {
        _paymentMethod: {
          name: '_paymentMethod',
          type: 'PaymentMethod[]',
          model: 'PaymentMethod',
          relationType: 'embedsOne',
                  keyFrom: 'paymentMethod',
          keyTo: 'id'
        },
        company: {
          name: 'company',
          type: 'Company',
          model: 'Company',
          relationType: 'belongsTo',
                  keyFrom: 'companyId',
          keyTo: 'id'
        },
        users: {
          name: 'users',
          type: 'Users',
          model: 'Users',
          relationType: 'belongsTo',
                  keyFrom: 'validateBy',
          keyTo: 'id'
        },
      }
    }
  }
}
