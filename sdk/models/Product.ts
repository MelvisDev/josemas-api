/* tslint:disable */
import {
  Company,
  SaleGroup,
  ProductCategory,
  IncomeCategory
} from '../index';

declare var Object: any;
export interface ProductInterface {
  "name"?: string;
  "description"?: string;
  "imageUrl"?: string;
  "urlReference"?: string;
  "price"?: number;
  "cost"?: number;
  "stock"?: number;
  "tax"?: boolean;
  "active"?: boolean;
  "id"?: string;
  "companyId"?: string;
  "saleGroupId"?: string;
  "productCategoryId"?: string;
  "incomeCategoryId"?: string;
  "suppliers"?: Array<any>;
  "combo"?: Array<any>;
  "createdDate"?: Date;
  "createdBy"?: number;
  "lastModificationDate"?: Date;
  "lastModificationBy"?: number;
  company?: Company;
  saleGroup?: SaleGroup;
  productCategory?: ProductCategory;
  incomeCategory?: IncomeCategory;
  _suppliers?: any[];
  _combo?: any[];
}

export class Product implements ProductInterface {
  "name": string;
  "description": string;
  "imageUrl": string;
  "urlReference": string;
  "price": number;
  "cost": number;
  "stock": number;
  "tax": boolean;
  "active": boolean;
  "id": string;
  "companyId": string;
  "saleGroupId": string;
  "productCategoryId": string;
  "incomeCategoryId": string;
  "suppliers": Array<any>;
  "combo": Array<any>;
  "createdDate": Date;
  "createdBy": number;
  "lastModificationDate": Date;
  "lastModificationBy": number;
  company: Company;
  saleGroup: SaleGroup;
  productCategory: ProductCategory;
  incomeCategory: IncomeCategory;
  _suppliers: any[];
  _combo: any[];
  constructor(data?: ProductInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Product`.
   */
  public static getModelName() {
    return "Product";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Product for dynamic purposes.
  **/
  public static factory(data: ProductInterface): Product{
    return new Product(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Product',
      plural: 'Products',
      path: 'Products',
      idName: 'id',
      properties: {
        "name": {
          name: 'name',
          type: 'string'
        },
        "description": {
          name: 'description',
          type: 'string'
        },
        "imageUrl": {
          name: 'imageUrl',
          type: 'string'
        },
        "urlReference": {
          name: 'urlReference',
          type: 'string'
        },
        "price": {
          name: 'price',
          type: 'number'
        },
        "cost": {
          name: 'cost',
          type: 'number'
        },
        "stock": {
          name: 'stock',
          type: 'number'
        },
        "tax": {
          name: 'tax',
          type: 'boolean'
        },
        "active": {
          name: 'active',
          type: 'boolean'
        },
        "id": {
          name: 'id',
          type: 'string'
        },
        "companyId": {
          name: 'companyId',
          type: 'string'
        },
        "saleGroupId": {
          name: 'saleGroupId',
          type: 'string'
        },
        "productCategoryId": {
          name: 'productCategoryId',
          type: 'string'
        },
        "incomeCategoryId": {
          name: 'incomeCategoryId',
          type: 'string'
        },
        "suppliers": {
          name: 'suppliers',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "combo": {
          name: 'combo',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "createdDate": {
          name: 'createdDate',
          type: 'Date',
          default: new Date(0)
        },
        "createdBy": {
          name: 'createdBy',
          type: 'number',
          default: 0
        },
        "lastModificationDate": {
          name: 'lastModificationDate',
          type: 'Date',
          default: new Date(0)
        },
        "lastModificationBy": {
          name: 'lastModificationBy',
          type: 'number',
          default: 0
        },
      },
      relations: {
        company: {
          name: 'company',
          type: 'Company',
          model: 'Company',
          relationType: 'belongsTo',
                  keyFrom: 'companyId',
          keyTo: 'id'
        },
        saleGroup: {
          name: 'saleGroup',
          type: 'SaleGroup',
          model: 'SaleGroup',
          relationType: 'belongsTo',
                  keyFrom: 'saleGroupId',
          keyTo: 'id'
        },
        productCategory: {
          name: 'productCategory',
          type: 'ProductCategory',
          model: 'ProductCategory',
          relationType: 'belongsTo',
                  keyFrom: 'productCategoryId',
          keyTo: 'id'
        },
        incomeCategory: {
          name: 'incomeCategory',
          type: 'IncomeCategory',
          model: 'IncomeCategory',
          relationType: 'belongsTo',
                  keyFrom: 'incomeCategoryId',
          keyTo: 'id'
        },
        _suppliers: {
          name: '_suppliers',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'suppliers',
          keyTo: 'id'
        },
        _combo: {
          name: '_combo',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'combo',
          keyTo: 'id'
        },
      }
    }
  }
}
