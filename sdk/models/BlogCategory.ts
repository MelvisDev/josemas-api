/* tslint:disable */
import {
  Company,
  Blog
} from '../index';

declare var Object: any;
export interface BlogCategoryInterface {
  "id"?: string;
  "companyId"?: string;
  "createdDate"?: Date;
  "createdBy"?: number;
  "lastModificationDate"?: Date;
  "lastModificationBy"?: number;
  "languages"?: Array<any>;
  company?: Company;
  _languages?: any[];
  blogs?: Blog[];
}

export class BlogCategory implements BlogCategoryInterface {
  "id": string;
  "companyId": string;
  "createdDate": Date;
  "createdBy": number;
  "lastModificationDate": Date;
  "lastModificationBy": number;
  "languages": Array<any>;
  company: Company;
  _languages: any[];
  blogs: Blog[];
  constructor(data?: BlogCategoryInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `BlogCategory`.
   */
  public static getModelName() {
    return "BlogCategory";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of BlogCategory for dynamic purposes.
  **/
  public static factory(data: BlogCategoryInterface): BlogCategory{
    return new BlogCategory(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'BlogCategory',
      plural: 'BlogCategories',
      path: 'BlogCategories',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'string'
        },
        "companyId": {
          name: 'companyId',
          type: 'string'
        },
        "createdDate": {
          name: 'createdDate',
          type: 'Date',
          default: new Date(0)
        },
        "createdBy": {
          name: 'createdBy',
          type: 'number',
          default: 0
        },
        "lastModificationDate": {
          name: 'lastModificationDate',
          type: 'Date',
          default: new Date(0)
        },
        "lastModificationBy": {
          name: 'lastModificationBy',
          type: 'number',
          default: 0
        },
        "languages": {
          name: 'languages',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
      },
      relations: {
        company: {
          name: 'company',
          type: 'Company',
          model: 'Company',
          relationType: 'belongsTo',
                  keyFrom: 'companyId',
          keyTo: 'id'
        },
        _languages: {
          name: '_languages',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'languages',
          keyTo: 'id'
        },
        blogs: {
          name: 'blogs',
          type: 'Blog[]',
          model: 'Blog',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'blogCategoryId'
        },
      }
    }
  }
}
