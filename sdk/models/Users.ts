/* tslint:disable */
import {
  Company,
  Receipt,
  Invoice,
  Membership,
  Evaluation,
  GeoPoint
} from '../index';

declare var Object: any;
export interface UsersInterface {
  "id"?: string;
  "firstName": string;
  "lastName": string;
  "birthdate"?: Date;
  "phoneNumber"?: string;
  "celphone"?: string;
  "documentNumber"?: string;
  "picture"?: string;
  "address"?: string;
  "location"?: GeoPoint;
  "locked"?: boolean;
  "active"?: boolean;
  "isCoach"?: boolean;
  "isOnlineCoach"?: boolean;
  "isEmployee"?: boolean;
  "isReceptionist"?: boolean;
  "isSalesman"?: boolean;
  "isSalesAdmin"?: boolean;
  "isSuperAdmin"?: boolean;
  "coachPercent"?: number;
  "facebookUrl"?: string;
  "instagramUrl"?: string;
  "youtubeUrl"?: string;
  "realm"?: string;
  "username"?: string;
  "email": string;
  "emailVerified"?: boolean;
  "companyId"?: string;
  "usersId"?: string;
  "membershipTypePrices"?: Array<any>;
  "healthInformation"?: Array<any>;
  "password"?: string;
  accessTokens?: any[];
  company?: Company;
  receipts?: Receipt[];
  coachAssistant?: Users;
  _membershipTypePrices?: any[];
  _healthInformation?: any[];
  invoices?: Invoice[];
  memberships?: Membership[];
  evaluationEvaluationsWorkoutRoutineCalendarWorkoutCategorySetsExercises?: any;
  evaluations?: Evaluation[];
}

export class Users implements UsersInterface {
  "id": string;
  "firstName": string;
  "lastName": string;
  "birthdate": Date;
  "phoneNumber": string;
  "celphone": string;
  "documentNumber": string;
  "picture": string;
  "address": string;
  "location": GeoPoint;
  "locked": boolean;
  "active": boolean;
  "isCoach": boolean;
  "isOnlineCoach": boolean;
  "isEmployee": boolean;
  "isReceptionist": boolean;
  "isSalesman": boolean;
  "isSalesAdmin": boolean;
  "isSuperAdmin": boolean;
  "coachPercent": number;
  "facebookUrl": string;
  "instagramUrl": string;
  "youtubeUrl": string;
  "realm": string;
  "username": string;
  "email": string;
  "emailVerified": boolean;
  "companyId": string;
  "usersId": string;
  "membershipTypePrices": Array<any>;
  "healthInformation": Array<any>;
  "password": string;
  accessTokens: any[];
  company: Company;
  receipts: Receipt[];
  coachAssistant: Users;
  _membershipTypePrices: any[];
  _healthInformation: any[];
  invoices: Invoice[];
  memberships: Membership[];
  evaluationEvaluationsWorkoutRoutineCalendarWorkoutCategorySetsExercises: any;
  evaluations: Evaluation[];
  constructor(data?: UsersInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Users`.
   */
  public static getModelName() {
    return "Users";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Users for dynamic purposes.
  **/
  public static factory(data: UsersInterface): Users{
    return new Users(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Users',
      plural: 'Users',
      path: 'Users',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'string'
        },
        "firstName": {
          name: 'firstName',
          type: 'string'
        },
        "lastName": {
          name: 'lastName',
          type: 'string'
        },
        "birthdate": {
          name: 'birthdate',
          type: 'Date'
        },
        "phoneNumber": {
          name: 'phoneNumber',
          type: 'string'
        },
        "celphone": {
          name: 'celphone',
          type: 'string'
        },
        "documentNumber": {
          name: 'documentNumber',
          type: 'string'
        },
        "picture": {
          name: 'picture',
          type: 'string'
        },
        "address": {
          name: 'address',
          type: 'string'
        },
        "location": {
          name: 'location',
          type: 'GeoPoint'
        },
        "locked": {
          name: 'locked',
          type: 'boolean'
        },
        "active": {
          name: 'active',
          type: 'boolean'
        },
        "isCoach": {
          name: 'isCoach',
          type: 'boolean'
        },
        "isOnlineCoach": {
          name: 'isOnlineCoach',
          type: 'boolean'
        },
        "isEmployee": {
          name: 'isEmployee',
          type: 'boolean'
        },
        "isReceptionist": {
          name: 'isReceptionist',
          type: 'boolean'
        },
        "isSalesman": {
          name: 'isSalesman',
          type: 'boolean'
        },
        "isSalesAdmin": {
          name: 'isSalesAdmin',
          type: 'boolean'
        },
        "isSuperAdmin": {
          name: 'isSuperAdmin',
          type: 'boolean'
        },
        "coachPercent": {
          name: 'coachPercent',
          type: 'number'
        },
        "facebookUrl": {
          name: 'facebookUrl',
          type: 'string'
        },
        "instagramUrl": {
          name: 'instagramUrl',
          type: 'string'
        },
        "youtubeUrl": {
          name: 'youtubeUrl',
          type: 'string'
        },
        "realm": {
          name: 'realm',
          type: 'string'
        },
        "username": {
          name: 'username',
          type: 'string'
        },
        "email": {
          name: 'email',
          type: 'string'
        },
        "emailVerified": {
          name: 'emailVerified',
          type: 'boolean'
        },
        "companyId": {
          name: 'companyId',
          type: 'string'
        },
        "usersId": {
          name: 'usersId',
          type: 'string'
        },
        "membershipTypePrices": {
          name: 'membershipTypePrices',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "healthInformation": {
          name: 'healthInformation',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "password": {
          name: 'password',
          type: 'string'
        },
      },
      relations: {
        accessTokens: {
          name: 'accessTokens',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'userId'
        },
        company: {
          name: 'company',
          type: 'Company',
          model: 'Company',
          relationType: 'belongsTo',
                  keyFrom: 'companyId',
          keyTo: 'id'
        },
        receipts: {
          name: 'receipts',
          type: 'Receipt[]',
          model: 'Receipt',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'usersId'
        },
        coachAssistant: {
          name: 'coachAssistant',
          type: 'Users',
          model: 'Users',
          relationType: 'hasOne',
                  keyFrom: 'id',
          keyTo: 'usersId'
        },
        _membershipTypePrices: {
          name: '_membershipTypePrices',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'membershipTypePrices',
          keyTo: 'id'
        },
        _healthInformation: {
          name: '_healthInformation',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'healthInformation',
          keyTo: 'id'
        },
        invoices: {
          name: 'invoices',
          type: 'Invoice[]',
          model: 'Invoice',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'usersId'
        },
        memberships: {
          name: 'memberships',
          type: 'Membership[]',
          model: 'Membership',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'usersId'
        },
        evaluationEvaluationsWorkoutRoutineCalendarWorkoutCategorySetsExercises: {
          name: 'evaluationEvaluationsWorkoutRoutineCalendarWorkoutCategorySetsExercises',
          type: 'any',
          model: '',
          relationType: 'hasOne',
                  keyFrom: 'id',
          keyTo: 'usersId'
        },
        evaluations: {
          name: 'evaluations',
          type: 'Evaluation[]',
          model: 'Evaluation',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'usersId'
        },
      }
    }
  }
}
