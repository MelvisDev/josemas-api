/* tslint:disable */
import {
  Company
} from '../index';

declare var Object: any;
export interface BodyPartInterface {
  "id"?: string;
  "companyId"?: string;
  "createdDate"?: Date;
  "createdBy"?: number;
  "lastModificationDate"?: Date;
  "lastModificationBy"?: number;
  "languages"?: Array<any>;
  company?: Company;
  language?: any[];
}

export class BodyPart implements BodyPartInterface {
  "id": string;
  "companyId": string;
  "createdDate": Date;
  "createdBy": number;
  "lastModificationDate": Date;
  "lastModificationBy": number;
  "languages": Array<any>;
  company: Company;
  language: any[];
  constructor(data?: BodyPartInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `BodyPart`.
   */
  public static getModelName() {
    return "BodyPart";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of BodyPart for dynamic purposes.
  **/
  public static factory(data: BodyPartInterface): BodyPart{
    return new BodyPart(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'BodyPart',
      plural: 'BodyParts',
      path: 'BodyParts',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'string'
        },
        "companyId": {
          name: 'companyId',
          type: 'string'
        },
        "createdDate": {
          name: 'createdDate',
          type: 'Date',
          default: new Date(0)
        },
        "createdBy": {
          name: 'createdBy',
          type: 'number',
          default: 0
        },
        "lastModificationDate": {
          name: 'lastModificationDate',
          type: 'Date',
          default: new Date(0)
        },
        "lastModificationBy": {
          name: 'lastModificationBy',
          type: 'number',
          default: 0
        },
        "languages": {
          name: 'languages',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
      },
      relations: {
        company: {
          name: 'company',
          type: 'Company',
          model: 'Company',
          relationType: 'belongsTo',
                  keyFrom: 'companyId',
          keyTo: 'id'
        },
        language: {
          name: 'language',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'languages',
          keyTo: 'id'
        },
      }
    }
  }
}
