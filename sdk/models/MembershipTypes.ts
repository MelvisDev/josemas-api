/* tslint:disable */
import {
  Company
} from '../index';

declare var Object: any;
export interface MembershipTypesInterface {
  "name"?: string;
  "price"?: number;
  "useCoach"?: boolean;
  "isGeneral"?: boolean;
  "isGeneralIncluded"?: boolean;
  "id"?: string;
  "companyId"?: string;
  "createdDate"?: Date;
  "createdBy"?: number;
  "lastModificationDate"?: Date;
  "lastModificationBy"?: number;
  company?: Company;
  userMembershipTypePrices?: any[];
}

export class MembershipTypes implements MembershipTypesInterface {
  "name": string;
  "price": number;
  "useCoach": boolean;
  "isGeneral": boolean;
  "isGeneralIncluded": boolean;
  "id": string;
  "companyId": string;
  "createdDate": Date;
  "createdBy": number;
  "lastModificationDate": Date;
  "lastModificationBy": number;
  company: Company;
  userMembershipTypePrices: any[];
  constructor(data?: MembershipTypesInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `MembershipTypes`.
   */
  public static getModelName() {
    return "MembershipTypes";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of MembershipTypes for dynamic purposes.
  **/
  public static factory(data: MembershipTypesInterface): MembershipTypes{
    return new MembershipTypes(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'MembershipTypes',
      plural: 'MembershipTypes',
      path: 'MembershipTypes',
      idName: 'id',
      properties: {
        "name": {
          name: 'name',
          type: 'string'
        },
        "price": {
          name: 'price',
          type: 'number'
        },
        "useCoach": {
          name: 'useCoach',
          type: 'boolean'
        },
        "isGeneral": {
          name: 'isGeneral',
          type: 'boolean'
        },
        "isGeneralIncluded": {
          name: 'isGeneralIncluded',
          type: 'boolean'
        },
        "id": {
          name: 'id',
          type: 'string'
        },
        "companyId": {
          name: 'companyId',
          type: 'string'
        },
        "createdDate": {
          name: 'createdDate',
          type: 'Date',
          default: new Date(0)
        },
        "createdBy": {
          name: 'createdBy',
          type: 'number',
          default: 0
        },
        "lastModificationDate": {
          name: 'lastModificationDate',
          type: 'Date',
          default: new Date(0)
        },
        "lastModificationBy": {
          name: 'lastModificationBy',
          type: 'number',
          default: 0
        },
      },
      relations: {
        company: {
          name: 'company',
          type: 'Company',
          model: 'Company',
          relationType: 'belongsTo',
                  keyFrom: 'companyId',
          keyTo: 'id'
        },
        userMembershipTypePrices: {
          name: 'userMembershipTypePrices',
          type: 'any[]',
          model: '',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'membershipTypesId'
        },
      }
    }
  }
}
