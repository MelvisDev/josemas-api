/* tslint:disable */
import {
  Company
} from '../index';

declare var Object: any;
export interface DietInterface {
  "name"?: string;
  "imageUrl"?: string;
  "weeks"?: number;
  "id"?: string;
  "avoid"?: Array<any>;
  "days"?: Array<any>;
  "companyId"?: string;
  "createdDate"?: Date;
  "createdBy"?: number;
  "lastModificationDate"?: Date;
  "lastModificationBy"?: number;
  _avoid?: any[];
  _days?: any[];
  company?: Company;
}

export class Diet implements DietInterface {
  "name": string;
  "imageUrl": string;
  "weeks": number;
  "id": string;
  "avoid": Array<any>;
  "days": Array<any>;
  "companyId": string;
  "createdDate": Date;
  "createdBy": number;
  "lastModificationDate": Date;
  "lastModificationBy": number;
  _avoid: any[];
  _days: any[];
  company: Company;
  constructor(data?: DietInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Diet`.
   */
  public static getModelName() {
    return "Diet";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Diet for dynamic purposes.
  **/
  public static factory(data: DietInterface): Diet{
    return new Diet(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Diet',
      plural: 'Diets',
      path: 'Diets',
      idName: 'id',
      properties: {
        "name": {
          name: 'name',
          type: 'string'
        },
        "imageUrl": {
          name: 'imageUrl',
          type: 'string'
        },
        "weeks": {
          name: 'weeks',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'string'
        },
        "avoid": {
          name: 'avoid',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "days": {
          name: 'days',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "companyId": {
          name: 'companyId',
          type: 'string'
        },
        "createdDate": {
          name: 'createdDate',
          type: 'Date',
          default: new Date(0)
        },
        "createdBy": {
          name: 'createdBy',
          type: 'number',
          default: 0
        },
        "lastModificationDate": {
          name: 'lastModificationDate',
          type: 'Date',
          default: new Date(0)
        },
        "lastModificationBy": {
          name: 'lastModificationBy',
          type: 'number',
          default: 0
        },
      },
      relations: {
        _avoid: {
          name: '_avoid',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'avoid',
          keyTo: 'id'
        },
        _days: {
          name: '_days',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'days',
          keyTo: 'id'
        },
        company: {
          name: 'company',
          type: 'Company',
          model: 'Company',
          relationType: 'belongsTo',
                  keyFrom: 'companyId',
          keyTo: 'id'
        },
      }
    }
  }
}
