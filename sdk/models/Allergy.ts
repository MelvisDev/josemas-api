/* tslint:disable */
import {
  Company
} from '../index';

declare var Object: any;
export interface AllergyInterface {
  "id"?: string;
  "languages"?: Array<any>;
  "companyId"?: string;
  "createdDate"?: Date;
  "createdBy"?: number;
  "lastModificationDate"?: Date;
  "lastModificationBy"?: number;
  language?: any[];
  company?: Company;
  companies?: Company[];
}

export class Allergy implements AllergyInterface {
  "id": string;
  "languages": Array<any>;
  "companyId": string;
  "createdDate": Date;
  "createdBy": number;
  "lastModificationDate": Date;
  "lastModificationBy": number;
  language: any[];
  company: Company;
  companies: Company[];
  constructor(data?: AllergyInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Allergy`.
   */
  public static getModelName() {
    return "Allergy";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Allergy for dynamic purposes.
  **/
  public static factory(data: AllergyInterface): Allergy{
    return new Allergy(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Allergy',
      plural: 'Allergies',
      path: 'Allergies',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'string'
        },
        "languages": {
          name: 'languages',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "companyId": {
          name: 'companyId',
          type: 'string'
        },
        "createdDate": {
          name: 'createdDate',
          type: 'Date',
          default: new Date(0)
        },
        "createdBy": {
          name: 'createdBy',
          type: 'number',
          default: 0
        },
        "lastModificationDate": {
          name: 'lastModificationDate',
          type: 'Date',
          default: new Date(0)
        },
        "lastModificationBy": {
          name: 'lastModificationBy',
          type: 'number',
          default: 0
        },
      },
      relations: {
        language: {
          name: 'language',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'languages',
          keyTo: 'id'
        },
        company: {
          name: 'company',
          type: 'Company',
          model: 'Company',
          relationType: 'belongsTo',
                  keyFrom: 'companyId',
          keyTo: 'id'
        },
        companies: {
          name: 'companies',
          type: 'Company[]',
          model: 'Company',
          relationType: 'hasMany',
          modelThrough: 'CompanyAllergy',
          keyThrough: 'companyId',
          keyFrom: 'id',
          keyTo: 'allergyId'
        },
      }
    }
  }
}
