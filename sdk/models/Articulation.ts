/* tslint:disable */

declare var Object: any;
export interface ArticulationInterface {
  "id"?: string;
  "languages"?: Array<any>;
  "createdDate"?: Date;
  "createdBy"?: number;
  "lastModificationDate"?: Date;
  "lastModificationBy"?: number;
  language?: any[];
}

export class Articulation implements ArticulationInterface {
  "id": string;
  "languages": Array<any>;
  "createdDate": Date;
  "createdBy": number;
  "lastModificationDate": Date;
  "lastModificationBy": number;
  language: any[];
  constructor(data?: ArticulationInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Articulation`.
   */
  public static getModelName() {
    return "Articulation";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Articulation for dynamic purposes.
  **/
  public static factory(data: ArticulationInterface): Articulation{
    return new Articulation(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Articulation',
      plural: 'Articulations',
      path: 'Articulations',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'string'
        },
        "languages": {
          name: 'languages',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "createdDate": {
          name: 'createdDate',
          type: 'Date',
          default: new Date(0)
        },
        "createdBy": {
          name: 'createdBy',
          type: 'number',
          default: 0
        },
        "lastModificationDate": {
          name: 'lastModificationDate',
          type: 'Date',
          default: new Date(0)
        },
        "lastModificationBy": {
          name: 'lastModificationBy',
          type: 'number',
          default: 0
        },
      },
      relations: {
        language: {
          name: 'language',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'languages',
          keyTo: 'id'
        },
      }
    }
  }
}
