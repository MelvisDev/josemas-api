/* tslint:disable */

declare var Object: any;
export interface RoutineInterface {
  "daysaWeek"?: number;
  "weeks"?: number;
  "tags"?: string;
  "urlPicture"?: string;
  "id"?: string;
  "days"?: Array<any>;
  "createdDate"?: Date;
  "createdBy"?: number;
  "lastModificationDate"?: Date;
  "lastModificationBy"?: number;
  "languages"?: Array<any>;
  day?: any[];
  language?: any[];
}

export class Routine implements RoutineInterface {
  "daysaWeek": number;
  "weeks": number;
  "tags": string;
  "urlPicture": string;
  "id": string;
  "days": Array<any>;
  "createdDate": Date;
  "createdBy": number;
  "lastModificationDate": Date;
  "lastModificationBy": number;
  "languages": Array<any>;
  day: any[];
  language: any[];
  constructor(data?: RoutineInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Routine`.
   */
  public static getModelName() {
    return "Routine";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Routine for dynamic purposes.
  **/
  public static factory(data: RoutineInterface): Routine{
    return new Routine(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Routine',
      plural: 'Routines',
      path: 'Routines',
      idName: 'id',
      properties: {
        "daysaWeek": {
          name: 'daysaWeek',
          type: 'number'
        },
        "weeks": {
          name: 'weeks',
          type: 'number'
        },
        "tags": {
          name: 'tags',
          type: 'string'
        },
        "urlPicture": {
          name: 'urlPicture',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'string'
        },
        "days": {
          name: 'days',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "createdDate": {
          name: 'createdDate',
          type: 'Date',
          default: new Date(0)
        },
        "createdBy": {
          name: 'createdBy',
          type: 'number',
          default: 0
        },
        "lastModificationDate": {
          name: 'lastModificationDate',
          type: 'Date',
          default: new Date(0)
        },
        "lastModificationBy": {
          name: 'lastModificationBy',
          type: 'number',
          default: 0
        },
        "languages": {
          name: 'languages',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
      },
      relations: {
        day: {
          name: 'day',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'days',
          keyTo: 'id'
        },
        language: {
          name: 'language',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'languages',
          keyTo: 'id'
        },
      }
    }
  }
}
