/* tslint:disable */
import {
  ExerciseCategory,
  Equipment
} from '../index';

declare var Object: any;
export interface ExerciseInterface {
  "useTempo"?: boolean;
  "urlPicture"?: string;
  "urlThumb"?: string;
  "visibleFor"?: string;
  "qrCode"?: string;
  "id"?: string;
  "category"?: any;
  "equipments"?: Array<any>;
  "createdDate"?: Date;
  "createdBy"?: number;
  "lastModificationDate"?: Date;
  "lastModificationBy"?: number;
  "languages"?: Array<any>;
  "bodyParts"?: Array<any>;
  "articulations"?: Array<any>;
  exerciseCategory?: ExerciseCategory[];
  equipment?: Equipment[];
  language?: any[];
  bodyPart?: any[];
  articulation?: any[];
}

export class Exercise implements ExerciseInterface {
  "useTempo": boolean;
  "urlPicture": string;
  "urlThumb": string;
  "visibleFor": string;
  "qrCode": string;
  "id": string;
  "category": any;
  "equipments": Array<any>;
  "createdDate": Date;
  "createdBy": number;
  "lastModificationDate": Date;
  "lastModificationBy": number;
  "languages": Array<any>;
  "bodyParts": Array<any>;
  "articulations": Array<any>;
  exerciseCategory: ExerciseCategory[];
  equipment: Equipment[];
  language: any[];
  bodyPart: any[];
  articulation: any[];
  constructor(data?: ExerciseInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Exercise`.
   */
  public static getModelName() {
    return "Exercise";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Exercise for dynamic purposes.
  **/
  public static factory(data: ExerciseInterface): Exercise{
    return new Exercise(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Exercise',
      plural: 'Exercises',
      path: 'Exercises',
      idName: 'id',
      properties: {
        "useTempo": {
          name: 'useTempo',
          type: 'boolean'
        },
        "urlPicture": {
          name: 'urlPicture',
          type: 'string'
        },
        "urlThumb": {
          name: 'urlThumb',
          type: 'string'
        },
        "visibleFor": {
          name: 'visibleFor',
          type: 'string'
        },
        "qrCode": {
          name: 'qrCode',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'string'
        },
        "category": {
          name: 'category',
          type: 'any'
        },
        "equipments": {
          name: 'equipments',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "createdDate": {
          name: 'createdDate',
          type: 'Date',
          default: new Date(0)
        },
        "createdBy": {
          name: 'createdBy',
          type: 'number',
          default: 0
        },
        "lastModificationDate": {
          name: 'lastModificationDate',
          type: 'Date',
          default: new Date(0)
        },
        "lastModificationBy": {
          name: 'lastModificationBy',
          type: 'number',
          default: 0
        },
        "languages": {
          name: 'languages',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "bodyParts": {
          name: 'bodyParts',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
        "articulations": {
          name: 'articulations',
          type: 'Array&lt;any&gt;',
          default: <any>[]
        },
      },
      relations: {
        exerciseCategory: {
          name: 'exerciseCategory',
          type: 'ExerciseCategory[]',
          model: 'ExerciseCategory',
          relationType: 'embedsOne',
                  keyFrom: 'category',
          keyTo: 'id'
        },
        equipment: {
          name: 'equipment',
          type: 'Equipment[]',
          model: 'Equipment',
          relationType: 'embedsMany',
                  keyFrom: 'equipments',
          keyTo: 'id'
        },
        language: {
          name: 'language',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'languages',
          keyTo: 'id'
        },
        bodyPart: {
          name: 'bodyPart',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'bodyParts',
          keyTo: 'id'
        },
        articulation: {
          name: 'articulation',
          type: 'any[]',
          model: '',
          relationType: 'embedsMany',
                  keyFrom: 'articulations',
          keyTo: 'id'
        },
      }
    }
  }
}
