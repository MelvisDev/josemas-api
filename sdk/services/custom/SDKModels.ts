/* tslint:disable */
import { Injectable } from '@angular/core';
import { Company } from '../../models/Company';
import { Illness } from '../../models/Illness';
import { Medication } from '../../models/Medication';
import { Allergy } from '../../models/Allergy';
import { BodyPart } from '../../models/BodyPart';
import { Equipment } from '../../models/Equipment';
import { Movement } from '../../models/Movement';
import { RoutineGoal } from '../../models/RoutineGoal';
import { RoutineDifficulty } from '../../models/RoutineDifficulty';
import { Articulation } from '../../models/Articulation';
import { ExerciseCategory } from '../../models/ExerciseCategory';
import { Exercise } from '../../models/Exercise';
import { Routine } from '../../models/Routine';
import { Food } from '../../models/Food';
import { Diet } from '../../models/Diet';
import { Suplier } from '../../models/Suplier';
import { SaleGroup } from '../../models/SaleGroup';
import { ProductCategory } from '../../models/ProductCategory';
import { PaymentMethod } from '../../models/PaymentMethod';
import { IncomeCategory } from '../../models/IncomeCategory';
import { Product } from '../../models/Product';
import { Receipt } from '../../models/Receipt';
import { Users } from '../../models/Users';
import { MembershipTypes } from '../../models/MembershipTypes';
import { Invoice } from '../../models/Invoice';
import { Membership } from '../../models/Membership';
import { Evaluation } from '../../models/Evaluation';
import { CompanyIllness } from '../../models/CompanyIllness';
import { CompanyAllergy } from '../../models/CompanyAllergy';
import { Email } from '../../models/Email';
import { BlogCategory } from '../../models/BlogCategory';
import { Blog } from '../../models/Blog';

export interface Models { [name: string]: any }

@Injectable()
export class SDKModels {

  private models: Models = {
    Company: Company,
    Illness: Illness,
    Medication: Medication,
    Allergy: Allergy,
    BodyPart: BodyPart,
    Equipment: Equipment,
    Movement: Movement,
    RoutineGoal: RoutineGoal,
    RoutineDifficulty: RoutineDifficulty,
    Articulation: Articulation,
    ExerciseCategory: ExerciseCategory,
    Exercise: Exercise,
    Routine: Routine,
    Food: Food,
    Diet: Diet,
    Suplier: Suplier,
    SaleGroup: SaleGroup,
    ProductCategory: ProductCategory,
    PaymentMethod: PaymentMethod,
    IncomeCategory: IncomeCategory,
    Product: Product,
    Receipt: Receipt,
    Users: Users,
    MembershipTypes: MembershipTypes,
    Invoice: Invoice,
    Membership: Membership,
    Evaluation: Evaluation,
    CompanyIllness: CompanyIllness,
    CompanyAllergy: CompanyAllergy,
    Email: Email,
    BlogCategory: BlogCategory,
    Blog: Blog,
    
  };

  public get(modelName: string): any {
    return this.models[modelName];
  }

  public getAll(): Models {
    return this.models;
  }

  public getModelNames(): string[] {
    return Object.keys(this.models);
  }
}
