'use strict';

module.exports = function(Equipment) {
	Equipment.disableRemoteMethodByName('image');
	Equipment.disableRemoteMethodByName('logo');
	Equipment.disableRemoteMethodByName('uploadImage');
};
