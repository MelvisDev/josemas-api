'use strict';

module.exports = function(Illness) {
	Illness.disableRemoteMethodByName('image');
	Illness.disableRemoteMethodByName('logo');
	Illness.disableRemoteMethodByName('uploadImage');
};
