'use strict';

module.exports = function(Diet) {
	Diet.disableRemoteMethodByName('image');
	Diet.disableRemoteMethodByName('logo');
};
