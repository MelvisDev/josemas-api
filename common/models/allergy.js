'use strict';

module.exports = function(Allergy) {
	Allergy.disableRemoteMethodByName('image');
	Allergy.disableRemoteMethodByName('logo');
	Allergy.disableRemoteMethodByName('uploadImage');
};
