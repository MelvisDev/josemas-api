'use strict';

module.exports = function(Bodypart) {
	Bodypart.disableRemoteMethodByName('image');
	Bodypart.disableRemoteMethodByName('logo');
	Bodypart.disableRemoteMethodByName('uploadImage');
};
