'use strict';

module.exports = function(Paymentmethod) {
	Paymentmethod.disableRemoteMethodByName('image');
	Paymentmethod.disableRemoteMethodByName('logo');
	Paymentmethod.disableRemoteMethodByName('uploadImage');
};
