'use strict';

module.exports = function(Routinedifficulty) {
	Routinedifficulty.disableRemoteMethodByName('image');
	Routinedifficulty.disableRemoteMethodByName('logo');
	Routinedifficulty.disableRemoteMethodByName('uploadImage');
};
