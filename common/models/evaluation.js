'use strict';
var loopback = require('loopback');
var formidable = require('formidable');
var fs = require('fs');
var AWS = require('aws-sdk');
var awsConfig = require('../../server/aws-config.json');
AWS.config.update(awsConfig);
var s3 = new AWS.S3();
var uuidv4 = require('uuid/v4');

module.exports = function(Evaluation) {
	Evaluation.disableRemoteMethodByName('image');
	Evaluation.disableRemoteMethodByName('logo');
	Evaluation.disableRemoteMethodByName('uploadImage');

	var err404 = new Error('Id not found');
	err404.statusText = 'Id not found';
	err404.statusCode = 404;

	Evaluation.observe('before save', function event(ctx, next) { //Observe any insert/update event on Model

		if (ctx.instance && ctx.isNewInstance) {
			// if(ctx.instance.evaluations){
			// 	if(ctx.instance.evaluations[0].notes.length > 1){
			// 		ctx.instance.evaluations[0].notes.map(function(note){
			// 			note.id = uuidv4();
			// 		});
			// 	}
			// }
		}
		next();
	});

	Evaluation.front = function(req, res, id, evaluationsId, cb) {
		// console.log(evaluationsId);
		// console.log('ID: ', id);
		Evaluation.findById(id, {
			include:{
				relation: '_evaluations',
				scope: {
					where: {
						id: evaluationsId
					}
				}
			}	
		}, function(err, succ){
			if(!succ) return cb(err404);
			if(succ._evaluations.length <= 0) return cb('_evaluationsId not found');

			uploadPicture(req, res, id, evaluationsId, cb, succ, 'evaluation', 'evaluations', 'front');
		});
	};

	Evaluation.right = function(req, res, id, evaluationsId, cb) {

		Evaluation.findById(id, {
			include:{
				relation: '_evaluations',
				scope: {
					where: {
						id: evaluationsId
					}
				}
			}	
		}, function(err, succ){
			if(!succ) return cb(err404);
			if(succ._evaluations.length <= 0) return cb('_evaluationsId not found');

			uploadPicture(req, res, id, evaluationsId, cb, succ, 'evaluation', 'evaluations', 'rightSide');
		});
	};

	Evaluation.left = function(req, res, id, evaluationsId, cb) {

		Evaluation.findById(id, {
			include:{
				relation: '_evaluations',
				scope: {
					where: {
						id: evaluationsId
					}
				}
			}	
		}, function(err, succ){
			if(!succ) return cb(err404);
			if(succ._evaluations.length <= 0) return cb('_evaluationsId not found');

			uploadPicture(req, res, id, evaluationsId, cb, succ, 'evaluation', 'evaluations', 'leftSide');
		});
	};

	Evaluation.back = function(req, res, id, evaluationsId, cb) {

		Evaluation.findById(id, {
			include:{
				relation: '_evaluations',
				scope: {
					where: {
						id: evaluationsId
					}
				}
			}	
		}, function(err, succ){
			// console	.log(err, succ);
			if(!succ) return cb(err404);
			if(succ._evaluations.length <= 0) return cb('_evaluationsId not found');
			
			uploadPicture(req, res, id, evaluationsId, cb, succ, 'evaluation', 'evaluations', 'back');
		});
	};

	Evaluation.attachments = function(req, res, id, evaluationsId, notes, cb) {

		Evaluation.findById(id, {
			include:{
				relation: '_evaluations',
				scope: {
					where: {
						id: evaluationsId
					}
				}
			}	
		}, function(err, succ){
			// console	.log(err, succ);
			if(!succ) return cb(err404);
			if(succ._evaluations.length <= 0) return cb('_evaluationsId not found');
			
			uploadFile(req, res, id, evaluationsId, cb, succ, 'evaluation', 'evaluations', notes);
		});
	};

	var uploadFile = function(req, res, id, evaluationsId, cb, modelData, currentModelName, relationModelName, notes){

		var form = new formidable.IncomingForm();

		form.parse(req, function(err, fields, files) {
			
			if (err) return cb(err);
			// console.log(files);
			if(!files.hasOwnProperty('')) return cb(null, 'File not Fount');
			var file = files[''];

			var currentBuckte = currentModelName ? awsConfig.bucket + '/' + currentModelName + '/' + id + '/' + relationModelName + '/' + evaluationsId : awsConfig.bucket;

			s3.putObject({
				Bucket: currentBuckte,
				Key: file.name,
				ACL:'public-read', 
				Body: fs.createReadStream(file.path),
				ContentType:file.type
			}, function(err,data) {
				if (err) return cb(err);
				var region = awsConfig.region ? awsConfig.region + '.' : '';
				var response = {
					originalName: file.name,
					type: file.type,
					link: 'https://S3.' + region + 'amazonaws.com/' + currentBuckte + '/' + file.name,
					date: new Date()
				}

				 console.log(evaluationsId);
				var newData = {
					evaluations: modelData.evaluations
				};

				var dataToInsert = {
					filename: file.name,
					url: response.link,
					type: file.type,
					date: response.date,
					notes: notes
				};

				var innerEvaluation = newData.evaluations.find(function(data){
						// console.log(data.id, evaluationsId);
						if(data.id == evaluationsId){
							return data;
						}
					});

				var fileExist = false;
				innerEvaluation.attachments.map(function(data){
					if(data.filename === file.name){
						data = dataToInsert;
						fileExist = true;
					}
				});

				if(!fileExist){
					innerEvaluation.attachments.push(dataToInsert);
				}

				newData.evaluations.map(function(data){
						if(data.id == innerEvaluation.id){
							data = innerEvaluation;
						}
					});	

				modelData.updateAttributes(newData, function(error, success){
					if(error) return cb(error);

					cb(null,response);
				})
			});
		});
	}

	var uploadPicture = function(req, res, id, evaluationsId, cb, modelData, currentModelName, relationModelName, fieldName){
		// console.log(evaluationsId)
		var form = new formidable.IncomingForm();
		form.parse(req, function(err, fields, files) {
			// console.log(files);
			if (err) return cb(err);
			if(!files.hasOwnProperty('')) return cb(null, 'File not Fount');
			var file = files[''];
			var filename = file.name;
			var ext = filename.substring(filename.lastIndexOf('.')+1, filename.length) || filename;
			filename = fieldName + '.' + ext;

			if(!file.name || file.name.match(/\.(jpg|jpeg|png)$/i)) {
				var currentBuckte = currentModelName ? awsConfig.bucket + '/' + currentModelName + '/' + id + '/' + relationModelName + '/' + evaluationsId : awsConfig.bucket;

				s3.putObject({
					Bucket: currentBuckte,
					Key: filename,
					ACL:'public-read', 
					Body: fs.createReadStream(file.path),
					ContentType:file.type
				}, function(err,data) {
					if (err) return cb(err);
					var region = awsConfig.region ? awsConfig.region + '.' : '';
					var response = {
						originalName: filename,
						type: file.type,
						link: 'https://S3.' + region + 'amazonaws.com/' + currentBuckte + '/' + filename,
						date: new Date()
					}

					// console.log(modelData.evaluations);
					var newData = {
						evaluations: modelData.evaluations
					};
					//console.log(modelData.evaluations[0]);
					var innerEvaluation = newData.evaluations.find(function(data){
						// console.log(data.id, evaluationsId);
						if(data.id == evaluationsId){
							return data;
						}
					});
					if(typeof innerEvaluation.pictures == 'undefined'){
						//console.log('paso');
						innerEvaluation.pictures = {};
					}
					innerEvaluation.pictures[fieldName] = response.link;

					newData.evaluations.map(function(data){
						if(data.id == innerEvaluation.id){
							data = innerEvaluation;
						}
					});	

					modelData.updateAttributes(newData, function(error, success){
						if(error) return cb(error);
						cb(null,response);
					})
				});
			}
			else {
				cb(file.name + ' is not allowed');
			}
		});
	}

	Evaluation.remoteMethod(
		'front',
		{
			isStatic: true,
			http: {path: '/:id/front/:evaluationsId', verb: 'post'},
			accepts: [
			{arg: 'req', type: 'object', 'http': {source: 'req'}},
			{arg: 'res', type: 'object', 'http': {source: 'res'}},
			{arg: 'id', type: 'string', required: true},
			{arg: 'evaluationsId', type: 'string', required: true},
			],
			returns: {arg: 'status', type: 'string'}
		});
	Evaluation.remoteMethod(
		'right',
		{
			isStatic: true,
			http: {path: '/:id/right/:evaluationsId', verb: 'post'},
			accepts: [
			{arg: 'req', type: 'object', 'http': {source: 'req'}},
			{arg: 'res', type: 'object', 'http': {source: 'res'}},
			{arg: 'id', type: 'string', required: true},
			{arg: 'evaluationsId', type: 'string', required: true},
			],
			returns: {arg: 'status', type: 'string'}
		});
	Evaluation.remoteMethod(
		'left',
		{
			isStatic: true,
			http: {path: '/:id/left/:evaluationsId', verb: 'post'},
			accepts: [
			{arg: 'req', type: 'object', 'http': {source: 'req'}},
			{arg: 'res', type: 'object', 'http': {source: 'res'}},
			{arg: 'id', type: 'string', required: true},
			{arg: 'evaluationsId', type: 'string', required: true},
			],
			returns: {arg: 'status', type: 'string'}
		});
	Evaluation.remoteMethod(
		'back',
		{
			isStatic: true,		
			http: {path: '/:id/back/:evaluationsId', verb: 'post'},
			accepts: [
			{arg: 'req', type: 'object', 'http': {source: 'req'}},
			{arg: 'res', type: 'object', 'http': {source: 'res'}},
			{arg: 'id', type: 'string', required: true},
			{arg: 'evaluationsId', type: 'string', required: true},
			],
			returns: {arg: 'status', type: 'string'}
		});
	Evaluation.remoteMethod(
		'attachments',
		{
			isStatic: true,		
			http: {path: '/:id/attachments/:evaluationsId/', verb: 'post'},
			accepts: [
			{arg: 'req', type: 'object', 'http': {source: 'req'}},
			{arg: 'res', type: 'object', 'http': {source: 'res'}},
			{arg: 'id', type: 'string', required: true},
			{arg: 'evaluationsId', type: 'string', required: true},
			{arg: 'notes', type: 'string', required: false},
			],
			returns: {arg: 'status', type: 'string'}
		});
};
