'use strict';

module.exports = function(Incomecategory) {
	Incomecategory.disableRemoteMethodByName('image');
	Incomecategory.disableRemoteMethodByName('logo');
	Incomecategory.disableRemoteMethodByName('uploadImage');
};
