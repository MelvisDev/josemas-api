'use strict';

module.exports = function(Exercise) {
	Exercise.disableRemoteMethodByName('image');
	Exercise.disableRemoteMethodByName('logo');

	Exercise.validatesInclusionOf('visibleFor', {
    	in: ['OnlyForMe', 'EverybodyInCompany', 'EmployeesOfCompany'], message: 'is not allowed'
	});
};
