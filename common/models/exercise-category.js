'use strict';

module.exports = function(Exercisecategory) {
	Exercisecategory.disableRemoteMethodByName('image');
	Exercisecategory.disableRemoteMethodByName('logo');
	Exercisecategory.disableRemoteMethodByName('uploadImage');
};
