'use strict';

module.exports = function(Salegroup) {
	Salegroup.disableRemoteMethodByName('image');
	Salegroup.disableRemoteMethodByName('logo');
	Salegroup.disableRemoteMethodByName('uploadImage');
};
