'use strict';

module.exports = function(Medication) {
	Medication.disableRemoteMethodByName('image');
	Medication.disableRemoteMethodByName('logo');
	Medication.disableRemoteMethodByName('uploadImage');
};
