'use strict';

module.exports = function(Routinegoal) {
	Routinegoal.disableRemoteMethodByName('image');
	Routinegoal.disableRemoteMethodByName('logo');
	Routinegoal.disableRemoteMethodByName('uploadImage');
};
