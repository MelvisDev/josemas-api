'use strict';
var loopback = require('loopback');
var formidable = require('formidable');
var fs = require('fs');
var AWS = require('aws-sdk');
var awsConfig = require('../../server/aws-config.json');
AWS.config.update(awsConfig);
var s3 = new AWS.S3();
var uuidv4 = require('uuid/v4');

module.exports = function(Base) {

	var err404 = new Error('Id not found');
	err404.statusText = 'Id not found';
	err404.statusCode = 404;

	Base.image = function(req, res, id, cb) {

		var currentModelName = this.definition.name;
		
		Model.findById(id, function(err, succ){
			if(!succ) return cb(err404);
			
			parseForm(req, res, id, cb, succ, currentModelName);
		});
	};

	Base.logo = function(req, res, id, cb) {

		var currentModelName = this.definition.name;
		var Model = loopback.getModel(currentModelName);

		Model.findById(id, function(err, succ){
			if(!succ) return cb(err404);
			
			parseForm(req, res, id, cb, succ, currentModelName, 'logo', 'logoUrl');
		});
	};

	Base.uploadImage = function(req, res, id, cb) {

		var currentModelName = this.definition.name;
		var Model = loopback.getModel(currentModelName);

		Model.findById(id, function(err, succ){
			if(!succ) return cb(err404);
			
			parseForm(req, res, id, cb, succ, currentModelName, 'image', 'imageUrl');
		});
	};

	var parseForm = function(req, res, id, cb, modelData, currentModelName, fileOptionalName, fieldName){

		var form = new formidable.IncomingForm();

		var images = (modelData.images) ? modelData.images : [];

		form.parse(req, function(err, fields, files) {
			if (err) return cb(err);
			var file = files[''];
			var filename = file.name;
			var ext = filename.substring(filename.lastIndexOf('.')+1, filename.length) || filename;
			filename = fileOptionalName ? fileOptionalName + '.' + ext : uuidv4() + '.' + ext;

			if(!file.name || file.name.match(/\.(jpg|jpeg|png)$/i)) {
				var currentBuckte = currentModelName ? awsConfig.bucket + '/' + currentModelName + '/' + id : awsConfig.bucket;

				s3.putObject({
					Bucket: currentBuckte,
					Key: filename,
					ACL:'public-read', 
					Body: fs.createReadStream(file.path),
					ContentType:file.type
				}, function(err,data) {
					if (err) return cb(err);
					var region = awsConfig.region ? awsConfig.region + '.' : '';
					var response = {
						originalName: filename,
						type: file.type,
						link: 'https://S3.' + region + 'amazonaws.com/' + currentBuckte + '/' + filename,
						date: new Date()
					}

					var dataToUpdate = {};
					if(fileOptionalName){
						dataToUpdate[fieldName] = response.link;
					}else{
						images.push({imageUrl: response.link});
						dataToUpdate = {images: images};
					}

					modelData.updateAttributes(dataToUpdate, function(error, success){
						if(error) return cb(error);

						cb(null,response);
					})
				});
			}
			else {
				cb(file.name + ' is not allowed');
			}
		});
	}

	Base.setup = function(){
		var BaseModel = this;		
		Base.base.setup.call(this);

		BaseModel.afterRemote('*.__get__company', function(ctx, inst, next) { 

			if(!ctx.instance.companyId){
				var Company = loopback.getModel('company');

				Company.find(function(error, succ){
					ctx.result = succ;
					next();
				});

			}else{
				next();
			}

		});

		BaseModel.remoteMethod(
			'image',
			{
				isStatic: true,
				http: {path: '/:id/image', verb: 'post'},
				accepts: [
				{arg: 'req', type: 'object', 'http': {source: 'req'}},
				{arg: 'res', type: 'object', 'http': {source: 'res'}},
				{arg: 'id', type: 'string'}
				],
				returns: {arg: 'status', type: 'string'}
			});
		BaseModel.remoteMethod(
			'logo',
			{
				isStatic: true,
				http: {path: '/:id/logo', verb: 'post'},
				accepts: [
				{arg: 'req', type: 'object', 'http': {source: 'req'}},
				{arg: 'res', type: 'object', 'http': {source: 'res'}},
				{arg: 'id', type: 'string'}
				],
				returns: {arg: 'status', type: 'string'}
			});

		BaseModel.remoteMethod(
			'uploadImage',
			{
				isStatic: true,
				http: {path: '/:id/uploadImage', verb: 'post'},
				accepts: [
				{arg: 'req', type: 'object', 'http': {source: 'req'}},
				{arg: 'res', type: 'object', 'http': {source: 'res'}},
				{arg: 'id', type: 'string'}
				],
				returns: {arg: 'status', type: 'string'}
			});
	}

	Base.setup();
};
