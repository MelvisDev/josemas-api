'use strict';

module.exports = function(Productcategory) {
	Productcategory.disableRemoteMethodByName('image');
	Productcategory.disableRemoteMethodByName('logo');
};
