'use strict';

module.exports = function(Membership) {
	Membership.disableRemoteMethodByName('image');
	Membership.disableRemoteMethodByName('logo');
	Membership.disableRemoteMethodByName('uploadImage');
};
