'use strict';

module.exports = function(Membershipduration) {
		Membershipduration.validatesInclusionOf('durationUnit', {
	    	in: ['MONTH', 'WEEKS', 'YEARS', 'TRAINING'], message: 'is not allowed'
		});
};
