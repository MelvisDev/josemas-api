'use strict';

module.exports = function(Receipt) {
	Receipt.disableRemoteMethodByName('image');
	Receipt.disableRemoteMethodByName('logo');
	Receipt.disableRemoteMethodByName('uploadImage');
};
