'use strict';

module.exports = function(Invoice) {
	Invoice.disableRemoteMethodByName('image');
	Invoice.disableRemoteMethodByName('logo');
	Invoice.disableRemoteMethodByName('uploadImage');
};
