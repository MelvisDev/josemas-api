'use strict';

module.exports = function(Food) {

	Food.disableRemoteMethodByName('image');
	Food.disableRemoteMethodByName('logo');

	Food.afterRemote('create',function(ctx,modellnstance,next){
		console.log(modellnstance);
		/*modelInstance.address.create(ctx.reg.body.address,function(err,result){
			if(err){
				modelInstance,destroy();
				next(err);
			}
			next();
		});*/
		next();
	});
};
