'use strict';

module.exports = function(Routine) {
	Routine.disableRemoteMethodByName('image');
	Routine.disableRemoteMethodByName('logo');
};
