'use strict';
var loopback = require('loopback');
var formidable = require('formidable');
var fs = require('fs');
var AWS = require('aws-sdk');
var awsConfig = require('../../server/aws-config.json');
AWS.config.update(awsConfig);
var s3 = new AWS.S3();
var uuidv4 = require('uuid/v4');
var config = require('../../server/config.json');
var path = require('path');
var senderAddress = "noreply@loopback.com"; //Replace this address with your actual address
var qs = require('querystring');

module.exports = function(Users) {
	var err404 = new Error('Id not found');
	err404.statusText = 'Id not found';
	err404.statusCode = 404;

	

	// Users.afterRemote('create', function(context, user, next) {
	// 	var options = {
	// 		type: 'email',
	// 		to: user.email,
	// 		from: senderAddress,
	// 		subject: 'Thanks for registering.',
	// 		template: path.resolve(__dirname, '../../server/views/verify.ejs'),
	// 		verifyHref: '',
	// 		host: 'localhost',
	// 		protocol: 'http',
	// 		port: 4200,
	// 		redirect: '/verified',
	// 		user: user
	// 	};

	// 	var pkName = Users.definition.idName() || 'id';

	// 	var displayPort = (
	// 		(options.protocol === 'http' && options.port == '80') ||
	// 		(options.protocol === 'https' && options.port == '443')
	// 		) ? '' : ':' + options.port;

	// 	var urlPath = '/#/auth/verify';

	// 	var verifyHref = options.protocol +
	// 	'://' +
	// 	options.host +
	// 	displayPort +
	// 	urlPath +
	// 	'?' + qs.stringify({
	// 		uid: '' + options.user[pkName],
	// 		redirect: options.redirect,
	// 	});

	// 	options.verifyHref = verifyHref;

	// 	user.verify(options, function(err, response) {
	// 		if (err) {
	// 			User.deleteById(user.id);
	// 			return next(err);
	// 		}
	// 		context.res.render('response', {
	// 			title: 'Signed up successfully',
	// 			content: 'Please check your email and click on the verification link ' +
	// 			'before logging in.',
	// 			redirectTo: '/',
	// 			redirectToLinkText: 'Log in'
	// 		});
	// 	});
	// });

	// // Method to render
	// Users.afterRemote('prototype.verify', function(context, user, next) {
	// 	context.res.render('response', {
	// 		title: 'A Link to reverify your identity has been sent '+
	// 		'to your email successfully',
	// 		content: 'Please check your email and click on the verification link '+
	// 		'before logging in',
	// 		redirectTo: '/',
	// 		redirectToLinkText: 'Log in'
	// 	});
	// });

	 //send password reset link when requested
	 Users.on('resetPasswordRequest', function(info) {
	 	var config = {
	 		host: 'localhost',
	 		port: 4200,
	 		urlPath: '/#/auth/reset-password'
	 	}
	 	var url = 'http://' + config.host + ':' + config.port + config.urlPath;
	 	var html = 'Click <a href="' + url + '?access_token=' +
	 	info.accessToken.id + '">here</a> to reset your password';

	 	Users.app.models.Email.send({
	 		to: info.email,
	 		from: senderAddress,
	 		subject: 'Password reset',
	 		html: html
	 	}, function(err) {
	 		if (err) return console.log('> error sending password reset email');
	 		console.log('> sending password reset email to:', info.email);
	 	});
	 });

	 Users.uploadImage = function(req, res, id, cb) {

	 	var currentModelName = this.definition.name;
	 	var Model = loopback.getModel(currentModelName);

	 	Model.findById(id, function(err, succ){
	 		if(!succ) return cb(err404);

	 		parseForm(req, res, id, cb, succ, currentModelName, 'image', 'picture');
	 	});
	 };

	 Users.remoteMethod(
	 	'uploadImage',
	 	{
	 		isStatic: true,
	 		http: {path: '/:id/uploadImage', verb: 'post'},
	 		accepts: [
	 		{arg: 'req', type: 'object', 'http': {source: 'req'}},
	 		{arg: 'res', type: 'object', 'http': {source: 'res'}},
	 		{arg: 'id', type: 'string'}
	 		],
	 		returns: {arg: 'status', type: 'string'}
	 	});

	 var parseForm = function(req, res, id, cb, modelData, currentModelName, fileOptionalName, fieldName){

	 	var form = new formidable.IncomingForm();

	 	var images = (modelData.images) ? modelData.images : [];

	 	form.parse(req, function(err, fields, files) {
	 		if (err) return cb(err);
	 		var file = files[''];
	 		var filename = file.name;
	 		var ext = filename.substring(filename.lastIndexOf('.')+1, filename.length) || filename;
	 		filename = fileOptionalName ? fileOptionalName + '.' + ext : uuidv4() + '.' + ext;

	 		if(!file.name || file.name.match(/\.(jpg|jpeg|png)$/i)) {
	 			var currentBuckte = currentModelName ? awsConfig.bucket + '/' + currentModelName + '/' + id : awsConfig.bucket;

	 			s3.putObject({
	 				Bucket: currentBuckte,
	 				Key: filename,
	 				ACL:'public-read', 
	 				Body: fs.createReadStream(file.path),
	 				ContentType:file.type
	 			}, function(err,data) {
	 				if (err) return cb(err);
	 				var region = awsConfig.region ? awsConfig.region + '.' : '';
	 				var response = {
	 					originalName: filename,
	 					type: file.type,
	 					link: 'https://S3.' + region + 'amazonaws.com/' + currentBuckte + '/' + filename,
	 					date: new Date()
	 				}

	 				var dataToUpdate = {};
	 				dataToUpdate[fieldName] = response.link;
	 				

	 				modelData.updateAttributes(dataToUpdate, function(error, success){
	 					if(error) return cb(error);

	 					cb(null,response);
	 				})
	 			});
	 		}
	 		else {
	 			cb(file.name + ' is not allowed');
	 		}
	 	});
	 }
	};
