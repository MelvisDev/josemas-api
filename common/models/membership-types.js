'use strict';

module.exports = function(Membershiptypes) {
	Membershiptypes.disableRemoteMethodByName('image');
	Membershiptypes.disableRemoteMethodByName('logo');
	Membershiptypes.disableRemoteMethodByName('uploadImage');
};
