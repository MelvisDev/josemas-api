'use strict';

module.exports = function(Membershipaccess) {
		Membershipaccess.validatesInclusionOf('accessType', {
	    	in: ['NO_RESTRICTION', 'LIMITED_ACCESS'], message: 'is not allowed'
		});
};
