'use strict';
var loopback = require('loopback');
var User = loopback.getModel('users');
var Product = loopback.getModel('product');

module.exports = function(Customsearch) {

    var response = {users:{}, products:{}};
    Customsearch.search = function(parameter, cb) {
        var pattern = new RegExp('.*'+parameter+'.*', "i"); /* case-insensitive RegExp search */
        User.find({
            where:{
                or:[
                    {firstName:{like: pattern}},
                    {lastName:{like: pattern}},
                    {phoneNumber:{like: pattern}},
                    {celphone:{like: pattern}},
                    {documentNumber:{like: pattern}},
                    {address:{like: pattern}},
                    {email:{like: pattern}}
                ]
            }
        },function(err, users){
            response.users = users;
            Product.find({
                where:{
                    or:[
                        {name:{like: pattern}},
                        {description:{like: pattern}}
                    ]
                }
            }, function(err, products){
                response.products = products;
                cb(null,response);
            })
        });
		
    };
    
    Customsearch.setup = function(){
		var CustomSearchModel = this;		
		Customsearch.base.setup.call(this);	

		CustomSearchModel.remoteMethod(
			'search',
			{
				isStatic: true,
				http: {path: '/search', verb: 'get'},
				accepts: [
				{arg: 'parameter', type: 'string'}
				],
				returns: {arg: 'status', type: 'string'}
			});
	}

	Customsearch.setup();
};
