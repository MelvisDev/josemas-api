'use strict';

module.exports = function(Articulation) {
	Articulation.disableRemoteMethodByName('image');
	Articulation.disableRemoteMethodByName('logo');
	Articulation.disableRemoteMethodByName('uploadImage');
};
