'use strict';

module.exports = function(Product) {
	Product.disableRemoteMethodByName('image');
	Product.disableRemoteMethodByName('logo');
};
