'use strict';

module.exports = function(Suplier) {
	Suplier.disableRemoteMethodByName('image');
	Suplier.disableRemoteMethodByName('uploadImage');
};
