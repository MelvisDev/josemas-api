'use strict';

module.exports = function(Movement) {
	Movement.disableRemoteMethodByName('image');
	Movement.disableRemoteMethodByName('logo');
	Movement.disableRemoteMethodByName('uploadImage');
};
