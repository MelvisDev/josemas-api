module.exports = function(Model, options) {

  Model.defineProperty('createdDate', {type: Date, default: null});
  Model.defineProperty('createdBy', {type: 'number', default: null});
  Model.defineProperty('lastModificationDate', {type: Date, default: null});
  Model.defineProperty('lastModificationBy', {type: 'number', default: null});

  Model.observe('before save', function event(ctx, next) { //Observe any insert/update event on Model
  	const token = ctx.options && ctx.options.accessToken;
    const userId = token && token.userId;

    if (ctx.instance && ctx.isNewInstance) {
    	ctx.instance.createdDate = new Date();
    	ctx.instance.createdBy = userId;
    }else if (ctx.instance) {
      ctx.instance.lastModificationDate = new Date();
      ctx.instance.lastModificationBy = userId;
    } else {
      ctx.data.lastModificationDate = new Date();
      ctx.data.lastModificationBy = userId;
    }
    next();
  });
}