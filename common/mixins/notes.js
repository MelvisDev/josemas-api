module.exports = function(Model, options) {

  Model.observe('before save', function event(ctx, next) { //Observe any insert/update event on Model
  	console.log('test');
    if (ctx.instance && ctx.isNewInstance) {
    	ctx.instance.date = new Date();
    }
    next();
  });
}